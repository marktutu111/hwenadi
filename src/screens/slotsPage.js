import React, { PureComponent,Component } from 'react'
import { View, Text, StyleSheet, FlatList, Dimensions,TextInput,StatusBar,ScrollView,TouchableOpacity } from 'react-native'
import NText from '../components/text'
import NButton from '../components/buttons/buttons'
import SlotItem from '../components/slotItem/slotItem'
import { GameConsumer } from '../store/gameplay.state';
const { height } = Dimensions.get('window');
import colors from '../constants/colors'
import CustomHeader from '../components/header/header';
import Icon from 'react-native-vector-icons/FontAwesome5';




class slotsPage extends Component {

    state={
        all:false
    }

    componentWillUnmount(){
        this.dispatch(
            {
                selected_slots:[],
                slotNumber:[],
                odds:[],
                reservations:[],
                onsearch:false,
                filter:[]
            },()=>this.setState({paymentPage:'SLOTS'})
        )
    }

    render(){

        const props=this.props;
        const renderSlots=(slots)=>slots.map((number,i)=>{
            return(
                <View key={i.toString()} style={{
                    maxWidth:50,
                    height:20,
                    borderRadius:100,
                    justifyContent:'center',
                    alignItems:'center',
                    backgroundColor:colors.yellow,
                    marginHorizontal:5,
                    paddingRight:5,
                    paddingLeft:5
                }}>
                    <Text style={{
                        color:'#fff',
                        fontSize:10,
                        fontWeight:'bold',
                        }}
                    >{number}</Text>
                </View>
            )
        });

        return (
            <View
                style={{
                    backgroundColor:'white',
                    paddingBottom:30,
                }}
            >
                <StatusBar barStyle="dark-content"/>
                <GameConsumer>
                    {
                        ({ state,select,selectAll,get_selected_slot,get_taken,dispath })=>{
    
                            const {productId,onsearch,filter}=state;
                            this.dispatch=dispath;

                            const slots=state.odds.map((slot,i)=>(
                                <SlotItem
                                    {...state}
                                    slot={slot}
                                    reservations={state.reservations}
                                    number={i + 1}
                                    amount={state?.betPrice}
                                    select={select}
                                    get_selected_slot={get_selected_slot}
                                    get_taken={get_taken}
                                />
                            ));
                            return (<>

                            <CustomHeader 
                                showBackButton
                                color={'#000'}
                                title="Slots"
                                search={true}
                                searchColor={'#000'}
                                searchIcon={ onsearch ? 'close':'search' }
                                onSearch={()=>{
                                    dispath(
                                        {
                                            onsearch:!onsearch,
                                            filter:[]
                                        }
                                    )
                                }}
                                onPressOut={
                                    ()=>this.props.navigation.goBack()
                                }
                            />

                            <View style={{
                                marginBottom:5,
                                marginLeft:20,
                                marginTop:10
                            }}>
                                <Text style={{fontSize:25}}>{productId?.name}</Text>
                            </View>

                            <TouchableOpacity 
                                style={{
                                    flexDirection:'row',
                                    alignItems:'center',
                                    marginBottom:20,
                                    marginLeft:20,
                                    width:'auto'
                                }}
                                onPress={()=>this.props.navigation.navigate('Participants',{ id:state['_id'] })}
                            >
                                <Icon 
                                    name="user"
                                    size={12}
                                    color={'green'}
                                    style={{marginRight:5}}
                                />
                                <Text style={{fontSize:17,color:'green'}}>View competitors</Text>
                                <Icon 
                                    name="chevron-right"
                                    size={15}
                                    color={'green'}
                                    style={{ marginLeft:5 }}
                                />
                            </TouchableOpacity>
                            <View style={styles.header}>                                
                                <NText style={styles.headerText}>TAP TO CHOOSE</NText>
                            </View>
                    
                            <ScrollView 
                                contentContainerStyle={{...styles.headerText,marginTop:5,height:30 }}
                                showsVerticalScrollIndicator={false}
                                showsHorizontalScrollIndicator={false}
                                horizontal
                            > 
                                {renderSlots(state.selected_slots)} 
                            </ScrollView>

                                { onsearch && <TextInput 
                                    style={{
                                        borderColor:'#eee',
                                        borderWidth:1,
                                        padding:10,
                                        marginTop:10,
                                        width:'92%',
                                        marginRight:'auto',
                                        marginLeft:'auto',
                                        borderRadius:5
                                    }}
                                    placeholder="search slot"
                                    onChangeText={
                                        (v)=>{
                                            let slots=[];
                                            state.odds.forEach((slot,i)=>{
                                                if(slot.toString().indexOf(v)>-1){
                                                    slots.push(
                                                        <SlotItem
                                                            {...state}
                                                            slot={slot}
                                                            reservations={state.reservations}
                                                            number={i+1}
                                                            amount={state?.betPrice}
                                                            select={
                                                                (d)=>{
                                                                    select(d)
                                                                    dispath(
                                                                        {
                                                                            onsearch:false
                                                                        }
                                                                    )
                                                                }
                                                            }
                                                            get_selected_slot={get_selected_slot}
                                                            get_taken={get_taken}
                                                        />
                                                    )
                                                }
                                            });
                                            dispath(
                                                {
                                                    filter:slots
                                                }
                                            )
                                        }
                                    }
                                />}
    
                                {state.selected_slots.length>0 && <View
                                    style={{
                                        flexDirection:'row-reverse',
                                        alignItems:'center',
                                        marginTop:10
                                    }}
                                >
                                    <NButton 
                                            onPress={()=>{
                                                selectAll(state.odds,()=>{
                                                    this.setState({
                                                        all:true
                                                    },()=>{
                                                        this.props.navigation.navigate('Payment');
                                                    })
                                                })
                                            }} 
                                            title='All' 
                                            width='30%'
                                            style={{alignSelf:'flex-end',margin: 5,marginRight:20}}
                                    />
                                    {this.state.all ? (
                                        <NButton 
                                            onPress={()=>selectAll([],()=>this.setState({ all:false }))} 
                                            title='Clear'
                                            width='30%' 
                                            style={{alignSelf:'flex-end',margin: 5}}
                                        />
                                    ):(<NButton 
                                        onPress={()=>this.props.navigation.navigate('Payment')} 
                                        title='Done'
                                        width='30%' 
                                        style={{alignSelf:'flex-end',margin: 5}}
                                    />)}
                                </View>}
                                <FlatList
                                    keyboardShouldPersistTaps='handled'
                                    contentContainerStyle={{
                                        justifyContent: 'space-between',
                                        paddingHorizontal: 18,
                                        marginTop:15,
                                        paddingBottom:60
                                    }}
                                    data={ onsearch ? filter:slots}
                                    numColumns={2}
                                    style={{marginBottom:100,backgroundColor:'#fff'}}
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={(x, i) => i}
                                    renderItem={(item) => {
                                        return item.item
                                    }}
                                />
                            </>
                            )
                        }
                    }
                </GameConsumer>
            </View>
        )
    }


}
const styles = StyleSheet.create({
    header: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerText: {
        fontSize:15,
        marginHorizontal:20,
        alignSelf: 'flex-start',
        marginBottom: 0,
        flexDirection:'row'
    },
})
export default slotsPage
