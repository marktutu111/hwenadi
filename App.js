import React, { Component } from 'react';
import { StatusBar, Platform } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import MainApp from './src/components/main-app/mainApp'
import Signin from './src/screens/sign-in'
import Signup from './src/screens/sign-up'
import PrivacyPolicy from './src/screens/privacy-policy'
import ProductSearch from './src/screens/product-search'
import OTP from './src/screens/OTP';
import UserGuide from "./src/screens/userguide";
import slotsPage from './src/screens/slotsPage';
import ParticipantsPage from "./src/screens/participants";

// states
import AuthState, { AuthConsumer } from "./src/store/auth.state";
import GamePlayState from './src/store/gameplay.state';
import Loader from './src/screens/loader';
import Notifications from "./src/services/notifications.service";
import PaymentPage from './src/screens/paymentPage';
import VerifyPayment from './src/screens/verify-payment';
import ResetPassword from './src/screens/resetpassword';
import DepositPage from './src/screens/deposit.page';
import WheelPage from "./src/screens/wheelpage";

const Stack = createStackNavigator();


StatusBar.setBarStyle("light-content");
if (Platform.OS === "android") {
  StatusBar.setBackgroundColor("rgba(0,0,0,0)");
  StatusBar.setTranslucent(true);
}

class App extends Component {

  componentDidMount(){
    if(Platform.OS==='ios'){
      Notifications.requestUserPermission();
    }
  }


  render() {
    return (
      <AuthState>
        <AuthConsumer>
          {
            ({ state }) => {
              if (state.user && state.user._id) {
                return (
                  <GamePlayState>
                    <NavigationContainer>
                      <Stack.Navigator>
                        <Stack.Screen 
                          name="MainApp" 
                          component={MainApp} 
                          options={{ headerShown: false }} 
                        />
                        <Stack.Screen 
                          name="Deposit" 
                          component={DepositPage} 
                          options={{ headerShown: false }} 
                        />
                        <Stack.Screen 
                          name="WheelPage" 
                          component={WheelPage} 
                          options={{ headerShown: false }} 
                        />
                        <Stack.Screen 
                          name="UserGuide" 
                          component={UserGuide} 
                          options={{ headerShown: false }} 
                        />
                        <Stack.Screen 
                          name="Privacy-policy" 
                          component={PrivacyPolicy}
                          options={{ headerShown: false }}
                        />
                        <Stack.Screen 
                          name="Slots" 
                          component={slotsPage}
                          options={{ headerShown: false }}
                        />
                        <Stack.Screen 
                          name="Participants" 
                          component={ParticipantsPage}
                          options={{ headerShown: false }}
                        />
                        <Stack.Screen 
                          name="Payment" 
                          component={PaymentPage}
                          options={{ headerShown: false }}
                        />
                        <Stack.Screen 
                          name="VerifyPayment" 
                          component={VerifyPayment}
                          options={{ headerShown: false }}
                        />
                        <Stack.Screen 
                          name="Product-Search" 
                          component={ProductSearch} 
                          options={{ 
                            title: '', 
                            headerShown:false
                          }} 
                        />
                        {/* <Stack.Screen name="ButtomDrawer" component={ButtomDrawer} options={{ headerShown: false }} /> */}
                      </Stack.Navigator>
                    </NavigationContainer>
                  </GamePlayState>
                )
              } else return (
                <NavigationContainer>
                  <Stack.Navigator>
                  <Stack.Screen 
                      name="Loader" 
                      component={Loader} 
                      options={{ 
                        headerShown: false 
                      }} 
                    />
                    <Stack.Screen 
                      name="Signin" 
                      component={Signin} 
                      options={{ 
                        headerShown: false 
                      }} 
                    />
                    <Stack.Screen 
                      name="Signup" 
                      component={Signup} 
                      options={{ 
                        headerShown: false 
                      }} 
                    />
                    <Stack.Screen 
                      name="ResetPassword" 
                      component={ResetPassword} 
                      options={{ 
                        headerShown: false 
                      }} 
                    />
                    <Stack.Screen 
                      name="OTP" 
                      component={OTP} 
                      options={{ 
                        headerShown: false 
                      }} 
                    />
                    <Stack.Screen 
                      name="Privacy-policy" 
                      component={PrivacyPolicy}
                      options={{ headerShown: false }}
                    />
                  </Stack.Navigator>
                </NavigationContainer>
              )
            }
          }
        </AuthConsumer>
      </AuthState>
    );
  }

};

export default App;
