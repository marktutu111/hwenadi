import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity,Linking,ScrollView,ActivityIndicator,Image } from 'react-native'
import CustomHeader from '../components/header/header';
import NText from '../components/text';
import colors from '../constants/colors';
import { AuthConsumer } from "../store/auth.state";
import Icon from "react-native-vector-icons/FontAwesome5";
import service from "../services/debosit.service";




export default class Profile extends Component {

    state={
        loading:false,
        points:0
    }

    componentDidMount(){
        this.getDeposit();
    }
    

    async getDeposit(){
        try {
            this.setState(
                {
                    loading:true
                }
            );
            const {_id}=this.user;
            const response=await service.getDeposit(_id);
            if(!response.success){
                throw Error(
                    'You dont have points to play. Kindly make deposit'
                )
            };
            const {points}=response.data;
            this.setState(
                {
                    loading:false,
                    points:points
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                }
            )
        }
    }

    
    
    render() {

        return (
            <AuthConsumer>
                {
                    ({state,dispath})=>{
                        this.user=state.user;
                        return (
                            <ScrollView onScrollEndDrag={this.getDeposit.bind(this)}  contentContainerStyle={styles.container}>
                                <CustomHeader 
                                    showLogo 
                                    title="Profile"
                                    onPressOut={
                                        ()=>this.props.navigation.navigate(
                                            'UserGuide'
                                        )
                                    }
                                />
                                <View 
                                    style={{
                                        alignItems:'center',
                                        width:'70%',
                                        alignSelf:'center',
                                        position:'absolute',
                                        top:'15%'
                                    }}
                                    >
                                        <View style={styles.logoContainer}>
                                    <Image style={{height:'100%',width:'100%'}} source={require('../assets/images/melcom-logo.png')}/>
                                </View>
                                    <View style={styles.item}>
                                        <NText style={styles.itemText}>Name</NText>
                                        <NText style={styles.itemText}>{this.user.name}</NText>
                                    </View>
                                    <View style={styles.item}>
                                        <NText style={styles.itemText}>Phone</NText>
                                        <NText style={styles.itemText}>{this.user.phone}</NText>
                                    </View>
                                    <View style={styles.depositItem}>
                                        <View style={{
                                            flexDirection:'row',
                                            alignItems:'center'
                                        }}>
                                            <Icon 
                                                name="coins"
                                                color="#fff"
                                                size={17}
                                                style={{
                                                    marginRight:5
                                                }}
                                            />
                                            <NText style={styles.itemText}>Points</NText>
                                        </View>
                                        {
                                            this.state.loading ? (
                                                <ActivityIndicator 
                                                    color='#fff'
                                                    size={'small'}
                                                />
                                            ):(
                                                <NText 
                                                    style={styles.itemText}
                                                >{this.state.points}</NText>
                                            )
                                        }
                                    </View>
                                    <View style={styles.ruler}/>
                                    <TouchableOpacity style={styles.button}>
                                        <Text style={{fontSize:20,color:'#fff'}}>Create Gift Card</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.button}>
                                        <Text style={{fontSize:20,color:'#fff'}}>Renew discount card</Text>
                                    </TouchableOpacity>
                                    {/* <View style={styles.ruler}/> */}
                                    <View style={{flexDirection:'row'}}>
                                    <TouchableOpacity 
                                            style={{ 
                                                alignSelf: 'flex-end',
                                                marginTop:20,
                                                borderWidth:1,
                                                borderColor:'white',
                                                borderRadius:20,
                                                padding:8,
                                                paddingHorizontal:15,
                                                marginRight:20
                                            }} onPress={()=>{
                                                AsyncStorage.removeItem(
                                                    'tonadi-user'
                                                ).then(()=>{
                                                    dispath(
                                                        {
                                                            user:null,
                                                            token:null
                                                        }
                                                    )
                                                })
                                            }}>
                                            <NText style={styles.txt}>Logout</NText>
                                        </TouchableOpacity>
                                        <TouchableOpacity 
                                            style={{
                                                alignSelf: 'flex-end',
                                                marginTop:20,
                                                borderWidth:1,
                                                borderColor:'white',
                                                borderRadius:20,
                                                padding:8,
                                                paddingHorizontal:15
                                            }} onPress={()=>this.props.navigation.navigate('Deposit')}>
                                            <NText style={styles.txt}>Make Deposit</NText>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <TouchableOpacity 
                                    style={styles.bottom} 
                                    onPress={()=> this.props.navigation.navigate('Privacy-policy')}
                                >
                                    <Text 
                                        style={{ color: '#000', fontWeight: '700' }}
                                    >Game Rules</Text>
                                </TouchableOpacity>
                            </ScrollView>
                        )
                    }
                }
            </AuthConsumer>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.yellow,
        alignItems: 'center',
        paddingBottom: 100,
    },
    item: {
        borderBottomWidth: 10,
        borderColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        width: '100%',
        justifyContent:'space-between',
        marginBottom: 7
    },
    depositItem: {
        borderRadius:10,
        borderWidth:1,
        borderColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        width: '100%',
        justifyContent:'space-between',
        marginBottom: 7
    },
    itemText:{
        color:'white',
        fontSize: 20
    },
    txt:{ fontSize: 18,color:'white' },
    header: {
        fontSize: 35,
        color: 'white',
        marginLeft: 22,
        alignSelf: 'flex-start'
    },
    bottom: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 50,
        backgroundColor: 'white',
        padding: 5,
        paddingHorizontal: 15,
        borderRadius: 25
    },
    ruler:{
        height:1,
        width:'100%',
        backgroundColor:'#fff',
        opacity:0.5,
        marginVertical:5
    },
    button:{
        textAlign:'center',
        justifyContent:'center',
        padding:10,
        backgroundColor:colors._yellow,
        marginVertical:5,
        borderRadius:5,
        height:55,
        width:'100%'
    },
    logoContainer:{
        justifyContent:'center',
        alignItems:'center',
        width:160,
        height:150,
        marginVertical:5
    }
})