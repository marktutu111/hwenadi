import React, { Component } from 'react'
import { View, StyleSheet, ScrollView ,Platform, ActivityIndicator, Image} from 'react-native';
import Product from '../components/product/product';
import service from "../services/gameplays.service";
import { GameConsumer } from "../store/gameplay.state";
import colors from '../constants/colors';
import CustomHeader from '../components/header/header';
import AsyncStorage from '@react-native-async-storage/async-storage';



export default class Home extends Component {

    viewLoaded=false;
    interval=null;

    state = {
        page:1,
        loading:false,
        onfilter:false
    }

    componentDidMount() {
        this.viewLoaded=true;
        this.showGuide();
        this.getgames();
        this.interval=setInterval(() => {
            this.getgames();
        }, 1000*10);
    };


    componentWillUnmount(){
        clearInterval(this.interval);
    }


    showGuide(){
        AsyncStorage.getItem('showguide').then(
            res=>{
                if(typeof res==='string' && res==='NO'){
                    return;
                }
                this.props.navigation.navigate(
                    'UserGuide'
                )
            }
        ).catch(err=>null);
    }



    async getgames(p=false) {
        try {
            const page=p?this.state.page:1;
            this.viewLoaded && this.setState({ loading: true });
            const response = await service.paginate(
                {
                    filter:{
                        active:true,
                        gameover:false,
                        game_type:'PD'
                    },
                    page:page,
                    limit:10
                }
            );
            if (!response.success) {
                throw Error(response.message);
            }
            const games=response.data;
            const data=p?[...this.games,...games]:games;
            return this.viewLoaded && this.dispath(
                {
                    games:data
                },
                ()=>this.setState(
                    {
                        loading:false,
                        page:games.length>10?page+1:page
                    }
                )
            )
        } catch (err) {
            this.viewLoaded && this.setState(
                { 
                    loading:false 
                }
            );
        }
    }

    


    render() {

        const isCloseToBottom=({layoutMeasurement, contentOffset, contentSize})=>{
            const paddingToBottom = 20;
            return layoutMeasurement.width + contentOffset.x >=
              contentSize.width - paddingToBottom;
        };

        return (
            <View style={styles.container}>
                <CustomHeader 
                    title="Tonadi"
                    onSearch={()=>this.props.navigation.navigate('Product-Search')}
                    search={true}
                    showLogo={true}
                    onPressOut={
                        ()=>{
                            this.dispath(
                                {
                                    onfilter:false
                                },
                                ()=>this.getgames(false).catch(err=>null)
                            )
                        }
                    }
                />
                <GameConsumer>
                    {
                        ({ dispath,state }) => {
                            this.dispath=dispath;
                            this.games=state.games;
                            return (
                                <ScrollView
                                    horizontal
                                    decelerationRate='fast'
                                    bounces={false}
                                    snapToInterval={270}
                                    showsVerticalScrollIndicator={false}
                                    showsHorizontalScrollIndicator={false}
                                    style={{ alignSelf: 'center', height: '100%',marginTop:50 }}
                                    scrollEventThrottle={400}
                                    onScroll={({nativeEvent})=>{
                                        if (isCloseToBottom(nativeEvent)) {
                                            this.getgames(true);
                                        }
                                    }}
                                >
                                    <View style={{ height: '100%', width: 45 }}></View>
                                    { this.state.loading && state.games.length===0 && <View style={{
                                        justifyContent:'center',
                                        alignItems:'center'
                                    }}>
                                        <ActivityIndicator 
                                            size={'large'}
                                            color='#fff'
                                        />
                                    </View>}
                                    {
                                        state.games.map((game, i)=>{
                                            return game['game_type']==='PD' && (
                                                <Product
                                                    {...game.productId}
                                                    slotsRemaining={game.betcount - game.reservations.length}
                                                    data={game}
                                                    key={i.toString()}
                                                    goTo={()=>{
                                                        this.dispath(
                                                            { ...game },
                                                            () => this.props.navigation.navigate(
                                                                'Slots'
                                                            )
                                                        )
                                                    }}
                                                />
                                            )
                                        })
                                    }
                                </ScrollView>
                            )
                        }
                    }
                </GameConsumer>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.yellow,
    },
    header: {
        fontSize: 35,
        color: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 35,
        marginLeft: 22,
        marginRight: 22,
        marginBottom: 40,
    },
    headerText: {
        fontSize: 35,
        color: 'white',
    },
    filter:Platform.select(
        {
            'ios':{
                marginRight:20
            }
        }
    )
})