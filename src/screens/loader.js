import React, { Component,Fragment } from 'react'
import { View, StyleSheet, ActivityIndicator } from 'react-native'
import colors from '../constants/colors'
import AsyncStorage from '@react-native-async-storage/async-storage';
import service from "../services/auth.service";
import {AuthConsumer} from "../store/auth.state";
import LogoContainer from '../components/logo/logo';



export default class Loader extends Component {

    viewLoaded=false;

    componentWillUnmount(){
        this.viewLoaded=false;
    }

    componentDidMount(){
        this.viewLoaded=true;
        this.loadUser();
    }


    loadUser(){
        AsyncStorage.getItem('tonadi-user').then(async data=>{
            try {
                if(typeof data==='string'){
                    const user=JSON.parse(data);
                    if(user.phone && user.password){
                        const response=await service.login(
                            {
                                phone:user.phone,
                                password:user.password
                            }
                        );
                        if(!response.success){
                            throw Error(
                                'user not found'
                            )
                        }
                        if(this.viewLoaded){
                            this.setHeaders(response.token);
                            return this.dispath(
                                {
                                    user:response.data,
                                }
                            )
                        }
                    }
                }
                throw Error('');
            } catch (err) {
                this.props.navigation.navigate('Signin');
            }
        })
    }


    render() {
        return (
            <View style={styles.container}>
                <AuthConsumer>
                    {
                        ({dispath,setHeaders,state})=>{
                            this.timeout=state.timeout;
                            this.dispath=dispath;
                            this.setHeaders=setHeaders;
                        }
                    }
                </AuthConsumer>
                <LogoContainer
                    source={require('../assets/images/LOGO_MELCOM.png')}
                    width={150}
                    height={150}
                />
                <ActivityIndicator 
                    size={'large'}
                    color="#fff"
                    style={{
                        marginTop:10
                    }}
                />
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.melcom_blue,
        alignItems: 'center',
        paddingVertical:'60%'
    },
    header: {
        marginBottom: 25,
        fontSize: 35,
        marginLeft: 22,
        alignSelf: 'flex-start'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width:'100%',
        marginVertical:20
    }
})