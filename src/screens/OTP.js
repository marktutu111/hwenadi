import React, { Component,Fragment } from 'react'
import { View, StyleSheet, TouchableOpacity, Alert,ScrollView } from 'react-native'
import NText from '../components/text'
import NButton from '../components/buttons/buttons'
import NTextInput from '../components/text-input'
import Icon from 'react-native-vector-icons/Ionicons';

import otpservice from '../services/otp.service';
import service from "../services/auth.service";
import { AuthConsumer } from '../store/auth.state';
import CustomHeader from '../components/header/header';
import colors from '../constants/colors'


export default class OTP extends Component {

    viewLoaded=false;

    state={
        loading:false,
        page:'SP',
        otp:''
    }

    componentDidMount(){
        this.viewLoaded=true;
        const d=this.props.route.params['data'];
        const page=this.props.route.params['page'];
        this.setState(
            {
                ...this.state,...d,
                page:page || 'SP'
            },()=>this.sendOtp()
        )
    }

    async sendOtp(resend=false){
        try {
            if(resend && this.viewLoaded){
                this.setState(
                    {
                        loading:true
                    }
                )
            }
            const response=await otpservice.sendOtp({phoneNumber:this.state.phone});
            if(!response.success){
                throw Error(response.message);
            }
            if(resend && this.viewLoaded){
                Alert.alert('OTP', 'Verification code has been sent to your phone')
                this.setState(
                    {
                        loading:false
                    }
                )
            }
        } catch (err) {
            if(resend && this.viewLoaded){
                Alert.alert('Oops!',err.message);
                this.setState(
                    {
                        loading:false
                    }
                )
            }
        }
    };

    sendRequest(){
        const{otp,page}=this.state;
        if(otp==='')return;
        switch (page) {
            case 'RP':
                return this.resetpassword();
            default:
                return this.signup();
        }
    }


    async resetpassword(){
        try {
            this.setState({loading:true});
            const{phone,password,otp}=this.state;
            let response=await service.reset(
                {
                    phone:phone,
                    password:password,
                    otp:otp
                }
            );
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            return this.setState(
                {
                    loading:false
                },
                ()=>{
                    Alert.alert('Yay!!', 'Your password reset was successful, please login to continue with your new password to continue.');
                    this.props.navigation.navigate('Signin');
                }
            )
        } catch (err) {
            this.setState({loading:false});
            Alert.alert(
                'Oops!',
                err.message
            )
        }
    }


    async signup(){
        try {
            const {phone,name,otp,password}=this.state;
            if(phone === '' || name==='') return this.props.navigation.navigate(
                'Signup'
            )
            this.setState({loading:true});
            let response=await service.signup({
                phone:phone,
                name:name,
                otp:otp,
                password:password
            });
            if(response===null){
                throw Error(
                    `Dear user,we cannot process your request now check your internet connection`
                )
            }
            if(!response.success){
                throw Error(response.message);
            }
            return this.setState(
                {
                    loading:false
                },
                ()=>{
                    Alert.alert('Yay!!', 'Your account has been successfully created, please login to continue');
                    this.props.navigation.navigate('Signin');
                }
            )
        } catch (err) {
            this.setState({loading:false});
            Alert.alert(
                'Oops!',
                err.message
            )
        }
    }

    render() {
        return (
            <View style={{backgroundColor:colors.yellow,height:'100%'}}>
                <AuthConsumer>
                    {
                        ({dispath})=>{
                            this.dispath=dispath;
                        }
                    }
                </AuthConsumer>
                <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps={'handled'}>
                    <CustomHeader 
                        title="OTP"
                        showBackButton
                        onPressOut={()=>this.props.navigation.goBack()}
                    />
                    <View style={{width:'90%',alignItems:'center',marginTop:'10%'}}>
                        <NText style={{color:'white',fontSize:18,marginBottom:10}}>We are sending you a verification code to verify your phone number</NText>
                        <View style={styles.row}>
                            <Icon name='chatbubble-ellipses-sharp' size={25} color='white' />
                            <TouchableOpacity onPress={()=>this.sendOtp(true)}>
                                <NText style={{color:'white'}}>     Resend SMS</NText>
                            </TouchableOpacity>
                        </View>
                        <NTextInput onChangeText={v=>this.state.otp=v.trim()} placeholder="OTP" width='100%' keyboardType='number-pad'/>
                        <NButton onPress={this.sendRequest.bind(this)} loading={this.state.loading} title='ENTER' width='100%' />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.yellow,
        alignItems: 'center',
    },
    header: {
        marginBottom: 25,
        fontSize: 35,
        marginLeft: 22,
        alignSelf: 'flex-start'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width:'100%',
        marginVertical:20
    }
})