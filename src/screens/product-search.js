import React, { Component, Fragment} from 'react'
import { View, StyleSheet, FlatList, Alert } from 'react-native';
import NButton from '../components/buttons/buttons';
import NTextInput from '../components/text-input';
import service from "../services/gameplays.service";
import { GameConsumer } from '../store/gameplay.state'
import colors from '../constants/colors';
import CustomHeader from '../components/header/header';




export default class ProductSearch extends Component {

    viewLoaded=false;
    
    state={
        loading:false,
        text:''
    }

    componentDidMount(){
        this.viewLoaded=true;
    }

    componentWillUnmount(){
        this.viewLoaded=false;
    }

    async filter() {
        try {
            this.setState({ loading: true });
            const response = await service.filterOngoing(this.state.text);
            if (!response.success) {
                throw Error(response.message);
            }
            return this.viewLoaded && this.dispath(
                {
                    games:response.data,
                    onfilter:true
                },
                ()=>this.props.navigation.goBack()
            )
        } catch (err) {
            this.viewLoaded && this.setState({ loading: false }, ()=>Alert.alert(
                'Oops!',
                'We couldnt find any product that matches your search'
            ));
        }
    }

    render() {
        return (
            <Fragment>
                <GameConsumer>
                    {
                        ({dispath})=>{
                            this.dispath=dispath
                        }
                    }
                </GameConsumer>
                <View style={styles.container}>
                    <CustomHeader 
                        title="Search Product"
                        showBackButton={true}
                        onPressOut={()=>this.props.navigation.goBack()}
                    />
                    <View style={{marginTop:50,width:'100%',justifyContent:'center',alignItems:'center'}}>    
                    <NTextInput 
                        onChangeText={v=>this.state.text=v.trim()} 
                        placeholder='eg: Television'
                        width="95%"
                    />
                    <NButton 
                        loading={this.state.loading}
                        title='Search' 
                        onPress={this.filter.bind(this)}
                        style={{width:'95%',marginTop:2}}
                    />
                    </View>
                </View>
            </Fragment>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.yellow,
        alignItems: 'center',
        paddingBottom: 100,
    },
    filterItem: {
        backgroundColor: 'black',
        borderRadius: 20,
        width: 130,
        height: 130,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 20,
        flex: 1
    },
    backBtn: { position: 'absolute', alignSelf: 'flex-start', marginLeft: 10 },

})