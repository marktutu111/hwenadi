import React, { PureComponent } from 'react'
import { View, Text, Dimensions, StyleSheet, Image,TouchableOpacity,ActivityIndicator } from 'react-native'
const { height,width } = Dimensions.get('window');
const _height = height * 0.65;
import moment from "moment";
import * as Progress from 'react-native-progress';
import colors from '../../constants/colors';



export default class extends PureComponent {

    state={
        loading:false
    }


    render(){

        const props=this.props;

        const bidPercent=(n,tn)=>((1*n)/tn).toFixed(2);
        const slotPurchased=props.data?.betcount-props.slotsRemaining;
        const bids=bidPercent(slotPurchased,props.data?.betcount);
    
        return (
            <TouchableOpacity activeOpacity={0.7} style={styles.container} onPress={()=>props.goTo()}>
                <View style={styles.linearGradient}></View>
                <View style={{
                    alignSelf: 'flex-start',
                    marginTop: 40,
                    marginLeft: 20,
                    position:'absolute',
                }}>
                    <Text style={{ 
                        color:colors.melcom_blue, 
                        fontSize: 20, 
                        // fontWeight:'bold'
                        }}
                    >{props.name.toUpperCase()}</Text>
                    <Text style={[
                        styles.txt, { color:colors.melcom_blue, fontSize: 15 }]}
                    >PAY JUST GHS{props.betPrice}</Text>
                    <Text style={styles.txt}>SLOTS LEFT: {slotPurchased}/{props.data?.betcount}</Text>
                    <View>
                        <Text style={{ fontSize:10,marginTop:5 }}>DATE: {moment(props.data?.createdAt).format('MM-DD-YYYY')}</Text>
                        <Text style={{ fontSize:10,marginTop:5 }}>{moment(props.data?.createdAt).fromNow()}</Text>
                    </View>
                </View>
                <View style={{
                    width: '90%',
                    position:'absolute',
                    bottom:40,
                    zIndex:-1,
                    borderTopRightRadius: 40,
                    borderTopLeftRadius: 40,
                    height: _height / 1.5,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop:5,
                    padding:5
                }}>
                    <Image
                    onLoadStart={()=>this.setState({loading:true})}
                    onLoadEnd={()=>this.setState({loading:false})}
                        source={{ uri: props.image }}
                        style={{
                            width: '85%',
                            height: '100%',
                        }}
                    />
                    {
                        this.state.loading && <ActivityIndicator
                            style={{ position:'absolute' }}
                            size={'large'}
                            color="red"
                        />
                    }
                </View>
    
                <View style={{
                    marginBottom:40,
                    flexDirection:'row',
                    alignItems:'center',
                    justifyContent:'space-around',
                    width:'70%'
                }}
                >
                    <Progress.Bar 
                        progress={bids || 0} 
                        width={150} 
                        height={10}
                        color={colors.melcom_blue}
                        style={{borderRadius:1,borderTopLeftRadius:5,borderBottomLeftRadius:5}}
                    />
                    <Text style={{color:colors.melcom_blue}}>{bids}%</Text>
                </View>
            </TouchableOpacity>
        )
    }


}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: _height,
        borderRadius: 40,
        width: width / 1.3,
        backgroundColor: 'white',
        marginRight: 20,
        alignItems: 'center',
        justifyContent: 'space-around',
        overflow: 'hidden',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    linearGradient: {
        // flex: 1,
        width:'100%',
        height:'100%',
        opacity:0.5,
        // position:'absolute'
        // paddingLeft: 15,
        // paddingRight: 15,
        // borderRadius: 5
      },
    txt: {
        // fontWeight: 'bold',
        fontSize: 16,
        marginTop:10
    }
})