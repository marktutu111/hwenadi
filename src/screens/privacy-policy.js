import React, { Component } from 'react'
import { StyleSheet,ScrollView, View,Text,StatusBar, Platform } from 'react-native'
import CustomHeader from '../components/header/header'
import NText from '../components/text'
import colors from '../constants/colors'



export default class PrivacyPolicy extends Component {
    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>

                <StatusBar  barStyle={'dark-content'}/>

                <CustomHeader 
                    showBackButton={true}
                    onPressOut={()=>this.props.navigation.goBack()}
                    title="General Rules"
                    headerFont={25}
                    color={'#000'}
                />
                <ScrollView contentContainerStyle={{alignItems:'flex-start', marginVertical:50,paddingHorizontal:15}}>
                    <Text style={styles.label}><Text style={styles.bold}>1.</Text> The customer enters his phone number and Password  created during the registration process. </Text>

                    <Text style={styles.label}><Text style={styles.bold}>2.</Text>When the customer has logged into the app successfully, The customer then searches or chooses the required product Interactively by tapping on the displayed products.</Text>

                    <Text style={styles.label}>
                    <Text style={styles.bold}>3.</Text> The customer can select one or multiple coupons and make payment. Each coupon has a price tag to tell the customer how much he has to spend per coupon. Customers cannot choose a coupon that has been chosen already. 
                    </Text>

                    <Text style={styles.label}><Text style={styles.bold}>4.</Text> Prior to the completion of the purchase, the customer must review and may edit his Selection, however once the customer has confirmed his Selection  and accepted the details, His Selection will be registered and the customer will not be able to cancel the process.</Text>

                    <Text style={styles.label}><Text style={styles.bold}>5.</Text> The total cost of the coupons selected will be displayed to the customer at the payment page.</Text>

                    <Text style={styles.label}><Text style={styles.bold}>6.</Text> The customer on the payment page is asked to enter mobile money details to authorize payment. An OTP is sent to the payment account and a prompt is issued to initiate the payment process.</Text>

                    <Text style={styles.label}><Text style={styles.bold}>7.</Text> Each successfully completed payment will be allocated a 
unique id by the central computer system.</Text>

                    <Text style={styles.label}><Text style={styles.bold}>8.</Text> All successfully completed payments and reservations will be acknowledged via SMS.</Text>


                    <Text style={styles.header}>GAME RESULTS</Text>

                    <Text style={styles.label}>One of the coupons is a win which is already generated randomly by the central computer system 
before the promotion starts hence the lucky winner is revealed after the promotion and when all the slots or coupons are purchased. 
An sms is sent with a code to the winner to come for the item. The losers are notified 
as well and asked to try again. </Text>


                    {Platform.OS=== 'ios' && <>
                        <Text style={styles.header}>CONCLUSION</Text>
                        <Text style={[styles.label,{ color: 'red',fontSize:20 }]}>Apple is not a participant in or sponsor of this promotion or contest and not involved in our activity in any manner.</Text>
                    </>}


                </ScrollView>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        // flex:1,
        // backgroundColor:colors.yellow,
        alignItems:'center'
    },
    header:{
        marginVertical:10,
        fontSize:20,
        fontWeight:'bold'
    },
    label:{
        marginVertical:10,
    },
    bold:{
        fontWeight:'bold',
        fontSize:15,
        padding:15
    }
})