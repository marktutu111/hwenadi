import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Alert, Image, ScrollView,KeyboardAvoidingView } from 'react-native'
import NText from '../components/text';
import NButton from '../components/buttons/buttons';
import NTextInput from '../components/text-input';
import NotificationService from "../services/notifications.service";

import service from "../services/auth.service";
import {AuthConsumer} from "../store/auth.state";
import colors from '../constants/colors';
import AsyncStorage from '@react-native-async-storage/async-storage';


export default class Signin extends Component {

    viewLoaded=false;

    state={
        phone:'',
        password:'',
        loading:false
    };

    componentDidMount(){
        this.viewLoaded=true;
    }

    componentWillUnmount(){
        this.viewLoaded=false;
    }

    async login(){
        try {

            const token= await NotificationService.getFcmToken();
            const {phone,password}=this.state;

            if(phone === '' || password==='') throw Error(
                'Please provide your phonenumber and password'
            );
            this.setState({loading:true});
            const data={
                phone:phone,
                password:password,
                token:token || null
            }
            const response=await service.login(data);
            if(response===null){
                throw Error(
                    `Dear user,we cannot process your request now check your internet connection`
                )
            }
            if(!response.success){
                throw Error(response.message);
            }
            if(this.viewLoaded){
                AsyncStorage.setItem('tonadi-user',JSON.stringify(data)).catch(
                    err=>null
                )
                this.setHeaders(response.token);
                this.dispath(
                    {
                        user:response.data
                    }
                )
            }
        } catch (err) {
            this.viewLoaded && this.setState({loading:false});
            Alert.alert(
                'Oops!',
                err.message
            )
        }
    }

    render() {
        return (
            <View style={{backgroundColor:colors.yellow,height:'100%'}} behavior="padding">    
                <ScrollView 
                    contentContainerStyle={styles.container} 
                    showsVerticalScrollIndicator={false} 
                    keyboardShouldPersistTaps='handled'
                >
                    <AuthConsumer>
                        {
                            ({dispath,setHeaders})=>{
                                this.dispath=dispath;
                                this.setHeaders=setHeaders;
                            }
                        }
                    </AuthConsumer>
                    <View style={{marginTop:'15%'}}>
                        <View style={{
                            width:100,
                            height:100,
                            justifyContent:'center',
                            alignItems:'center',
                            overflow: 'hidden',
                        }}>
                            <Image
                                style={{
                                    width:'100%',
                                    height:'100%'
                                }}
                                source={require('../assets/images/LOGO_MELCOM.png')}
                            />
                        </View>
                    </View>
                    <View style={styles.bottom}>
                        <TouchableOpacity 
                            style={{ alignSelf:'flex-end',marginRight:25,marginVertical:10 }} 
                            onPress={()=> this.props.navigation.navigate('Signup')}
                        >
                            <NText style={styles.txt} >Sign up</NText>
                        </TouchableOpacity>
                        <NTextInput 
                            keyboardType="phone-pad" 
                            maxLength={10}  
                            onChangeText={v=>this.state.phone=v.trim()} 
                            placeholder="Phone number" 
                            _style={styles.txtInpt} 
                        />
                        <NTextInput
                             secureTextEntry={true}
                            onChangeText={v=>this.state.password=v.trim()} 
                            placeholder="Password" 
                            _style={styles.txtInpt} 
                        />
                        <NButton 
                            loading={this.state.loading} 
                            onPress={this.login.bind(this)} 
                            title='LOGIN' 
                            width='90%' 
                            style={styles.btn}
                        />
                        <TouchableOpacity 
                            onPress={()=>this.props.navigation.navigate('ResetPassword')}
                            hitSlop={{
                                top:10,
                                bottom:10,
                                left:10,
                                right:10
                            }}
                            style={{
                                marginVertical:50,
                                borderRadius:5,
                                borderColor:'#fff',
                                borderWidth:1,
                                padding:5
                            }}
                        >
                            <Text style={{color:'#fff',fontSize:20}}>Reset Password</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 35,
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
    },
    bottom:{
        width: '100%', 
        bottom: 0, 
        alignItems: 'center',
        marginTop:'20%'
    },
    btn:{ marginBottom: 30,marginTop:10 },
    txtInpt:{ width: '90%' },
    txt:{ fontSize: 20,color:'#fff' }
})