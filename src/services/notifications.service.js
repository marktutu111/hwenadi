import messaging from '@react-native-firebase/messaging';


const requestUserPermission=async()=>{
  const authStatus = await messaging().requestPermission();
  const enabled =
    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    authStatus === messaging.AuthorizationStatus.PROVISIONAL;

  if (enabled) {
    console.log('Authorization status:', authStatus);
  }
}


const getForgroudMessagges=()=>{
  const unsubscribe=messaging().onMessage(async remoteMessage=>null);
  return unsubscribe;
}


const getFcmToken=async()=>{
  const fcmToken = await messaging().getToken();
  if (fcmToken) {
    return fcmToken;
  } else {
    return null;
  }
}


export default {
    requestUserPermission,
    getFcmToken,
    getForgroudMessagges
}