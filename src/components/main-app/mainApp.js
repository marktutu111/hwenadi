import React, {  PureComponent } from 'react'
import { StyleSheet, StatusBar } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Home from '../../screens/home'
import History from '../../screens/history'
import Profile from '../../screens/profile'
const Tab = createBottomTabNavigator();
import Animated from 'react-native-reanimated';
import { GameConsumer } from '../../store/gameplay.state';
import colors from '../../constants/colors';
import WinnersPage from '../../screens/winners';
import RewardsPage from '../../screens/rewards.page';

let fall = new Animated.Value(1);


export default class MainApp extends PureComponent {

    sheetRef;

    state={
        paymentPage:'SLOTS',
        bottomSheetHeight:0.55
    }

    render(){

        const renderShadow=()=>{
            const animatedShadowOpacity = Animated.interpolate(fall, {
                inputRange: [0, 1],
                outputRange: [0.5, 0],
            })
            return (
                <Animated.View
                    pointerEvents="none"
                    style={[
                        styles.shadowContainer,
                        {
                            opacity: animatedShadowOpacity,
                        },
                    ]}
                />
            )
        }
        
        StatusBar.setBarStyle("light-content");
        if (Platform.OS === "android"){
            StatusBar.setBackgroundColor("rgba(0,0,0,0)");
            StatusBar.setTranslucent(true);
        }
    
        return (<>
            <GameConsumer>
                {
                    ({dispath})=>{
                        this.dispath=dispath;
                    }
                }
            </GameConsumer>
                <Tab.Navigator
                    screenOptions={({ route }) => ({
                        tabBarIcon: ({ focused, color, size })=>{
                            let iconName;
                            switch (route.name) {
                                case 'Home':
                                    iconName='home';
                                    break;
                                case 'Wheel':
                                    iconName='dice';
                                    break;
                                case 'History':
                                    iconName='history';
                                    break;
                                case 'Winners':
                                    iconName='trophy';
                                    break;
                                default:
                                    iconName = 'user';
                                    break;
                            }
                            return <Icon name={iconName} size={25} color={color} />;
                        },
                    })}
                    tabBarOptions={{
                        activeTintColor: '#fff',
                        inactiveTintColor:colors.melcom_yellow,
                        showLabel:false,
                        style:{
                            elevation:0,
                            borderTopWidth:0,
                            backgroundColor:colors.yellow
                        }
                    }}
                >
                    <Tab.Screen 
                        name="Home"
                        component={Home}
                    />
                    <Tab.Screen 
                        name="Wheel"
                        component={RewardsPage}
                    />
                    <Tab.Screen 
                        name="Winners" 
                        options={{headerShown:false}}
                        component={WinnersPage} 
                    />
                    <Tab.Screen 
                        name="History" 
                        options={{headerShown:false}}
                        component={History} 
                    />
                    <Tab.Screen name="Profile" component={Profile} />
                </Tab.Navigator>
                {renderShadow()}
        
            </>
        )
    }
    

}

const styles = StyleSheet.create({
    shadowContainer: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: '#000',
    },
    nob: {
        width: 55,
        height: 12,
        borderRadius: 16,
        backgroundColor: '#ddd',
        alignSelf: 'center'
    },
    nobHeader: {
        bottom: -2,
        width: '100%',
        backgroundColor: 'white',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        paddingTop: 20,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 2,
        // },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 10,
    }
})
