import React, { PureComponent } from 'react'
import { View, Text, Dimensions, StyleSheet, Image,TouchableOpacity, Alert,ActivityIndicator } from 'react-native'
import colors from '../../constants/colors';
const { height,width } = Dimensions.get('window');
const _height = height * 0.65;
import { AuthConsumer } from '../../store/auth.state';


export default class Reward extends PureComponent {

    state={
        loading:false
    }


    render(){
        return (
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={styles.container} 
                onPress={this.props.goTo}>
                <AuthConsumer>
                    {
                        ({state})=>{
                            this.user=state[
                                'user'
                            ]
                        }
                    }
                </AuthConsumer>
                <View style={styles.linearGradient}></View>
                <View style={{
                    alignSelf: 'flex-start',
                    marginTop:20,
                    marginLeft:20,
                    position:'absolute',
                }}>
                    <Text style={{ 
                        color:colors.melcom_blue,
                        fontSize:20,
                    }}
                    >{this.props.reward?.name?.toUpperCase()}</Text>
                    <Text style={styles.txt}>WIN WITH JUST GHS1</Text>
                    <Text style={{ marginTop:5 }}>POINTS NEEDED: { this.props.maxPoints }</Text>
                    {/* <Text style={{ marginTop:5 }}>HIGHEST SCORE: { this.props.highestScore }</Text> */}
                    <Text style={{ marginTop:5 }}>TOTAL POINTS DRAWN: { this.props.totalPoints }</Text>
                </View>
                <View style={{
                    width: '85%',
                    position:'absolute',
                    bottom:20,
                    zIndex:-1,
                    height: _height / 1.8,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Image
                        onLoadStart={()=>this.setState({loading:true})}
                        onLoadEnd={()=>this.setState({loading:false})}
                        source={{ uri: this.props.reward?.label }}
                        style={{
                            width: '100%',
                            height: '100%',
                        }}
                    />
                    {
                        this.state.loading && <ActivityIndicator
                            style={{ position:'absolute' }}
                            size={'large'}
                            color="red"
                        />
                    }
                </View>
            </TouchableOpacity>
        )
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: _height,
        borderRadius:25,
        width: width / 1.3,
        backgroundColor: 'white',
        marginRight: 20,
        alignItems: 'center',
        justifyContent: 'space-around',
        overflow: 'hidden',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    linearGradient: {
        // flex: 1,
        width:'100%',
        height:'100%',
        opacity:0.5,
        // position:'absolute'
        // paddingLeft: 15,
        // paddingRight: 15,
        // borderRadius: 5
      },
    txt: {
        fontSize: 10,
        marginVertical:5,
        color:'#6d6d6d'
    }
})