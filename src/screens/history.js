import React, { Component } from 'react'
import { Text, View, StyleSheet,FlatList, ScrollView, ActivityIndicator,Platform,TouchableOpacity, TouchableHighlightBase } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import service from "../services/bet.service";
import { AuthConsumer } from "../store/auth.state";
import moment from "moment";
import colors from '../constants/colors';
import CustomHeader from '../components/header/header';
import { GameConsumer } from '../store/gameplay.state';
import gameService from "../services/gameplays.service";



class BetModel {

    rewardId;
    gameId;
    customerId;
    productId;
    slotNumber=[];
    productCode;
    won=false;
    betcode;
    createdAt;
    updatedAt;
    amount;
    winningSlot=0;
    numberOfSpins=0;
    score=0;

    constructor(bet){
        this.gameId=bet.gameId;
        this.customerId=bet.customerId;
        this.productId=bet.productId;
        this.slotNumber=bet.slotNumber;
        this.productCode=bet.productCode;
        this.won=bet.won;
        this.betcode=bet.betcode;
        this.createdAt=bet.createdAt;
        this.updatedAt=bet.updatedAt;
        this.amount=bet.amount;
        this.rewardId=bet.rewardId;
        this.score=bet.score;
        this.numberOfSpins=bet.numberOfSpins;

        this.getWinning()
            .catch(err=>null);

    };

    getgame(key='_id'){
        try {
            return this.gameId[key];
        } catch (err) {
            return null;
        }
    }

    getReward(key){
        try {
            return this.rewardId[key];
        } catch (err) {
            return null;
        }
    }

    getproduct(key){
        try {
            return this.productId[key];
        } catch (err) {
            return null;
        }
    }

    getRemaining(){
        return this.getgame('betcount')-this.getgame('reservations').length;
    };

    getSlots(){
        return this.slotNumber.map((number,i)=>{
            return(
                <View key={i.toString()} style={{
                    width:20,
                    height:20,
                    maxWidth:50,
                    borderRadius:100,
                    justifyContent:'center',
                    alignItems:'center',
                    backgroundColor:'#fff',
                    marginLeft:2,
                    paddingRight:5,
                    paddingLeft:5
                }}>
                    <Text style={{
                        color:'#000',
                        fontSize:10,
                        fontWeight:'bold'
                        }}
                    >{number}</Text>
                </View>
            )
        });
    };

    async getWinning(){
        try {
            const response=await gameService.getWinningSlot(this.getgame());
            if(!response.success){
                throw Error(response.message);
            }
            return this.winningSlot=response.data;
        } catch (err) {
            return 0;
        }
    }

}



const HistoryItem=({data,onPress})=>{
    return (
        <TouchableOpacity style={styles.historyItem} onPress={onPress} activeOpacity={0.7}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                {data.getgame('game_tye')==='PD' ?(<Text style={{...styles.txt, fontSize:20}}>
                    {data.getproduct && data.getproduct('name')}
                </Text>):(<Text style={{...styles.txt, fontSize:20}}>
                    {data.getproduct && data.getReward('name')}
                </Text>)}
                <View style={{ 
                    flexDirection: 'row', 
                    alignItems: 'center', 
                    justifyContent: 'space-around', 
                    paddingLeft:5,
                    paddingRight:5 
                }}>
                    {data.getgame('game_type')==='PD' ?<View style={styles.amount}>
                        <Text 
                            style={{ 
                                color:'#000' 
                            }}
                        >GHS{data.amount}</Text>
                    </View>:(
                        <View style={styles.amount}>
                            <Text 
                                style={{ 
                                    color:'#000' 
                                }}
                            >{data.score}</Text>
                        </View>
                    )}
                    <Icon 
                        name='check-circle-o' 
                        color='white'
                        size={15}
                        style={{marginRight:5}}
                    />
                    <Icon 
                        name='chevron-right' 
                        color='#eee' 
                        size={15}
                    />
                </View>
            </View>
            <View style={{ 
                flexDirection: 'row', 
                justifyContent: 'space-between',
                marginTop:10
            }}>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <Icon style={{ marginRight:5 }} size={17} name="calendar" color="#fff"/>
                    <Text 
                        style={[
                            styles.txt, { 
                                fontSize: 16
                            }]}>
                            {moment(data.updatedAt).format('MMM Do YY').toUpperCase()}
                    </Text>
                </View>
                {
                    data?.won ?(
                        <View style={styles.status}>
                            <Text style={[
                                styles.txt, { 
                                    fontSize: 14
                                }]}>
                                <Icon name='star'/> won
                            </Text>
                        </View>
                    ):(
                        <View style={styles.status}>
                            <Text style={{ 
                                    fontSize: 12,
                                    color:'#fff',
                            }}>
                                {/* <Icon  name='circle' color='#000'/> */}
                                { data.getgame('gameover') ? 'lost':'ongoing' }
                            </Text>
                        </View>
                    )
                }
            </View>
            <Text 
                style={styles.txt}
            >
                <Text>PRODUCT CODE</Text>: {data.getgame('productCode')}
            </Text>
            {
                data.getgame('game_type')==='PD'?(
                    <>
                        <View style={{
                                flexDirection:'row',
                            }}>
                                <Text style={{color:'#fff'}}>YOUR SLOTS:</Text> 
                                {data.getSlots()}
                        </View>
                        <Text style={styles.txt}><Text >SLOTS REMAINING</Text>: {data.getRemaining()}</Text>
                    </>
                ):(
                    <>
                        <View style={{
                            flexDirection:'row',
                        }}>
                            <Text style={{color:'#fff'}}>WINNING SCORE: {data.getgame('maxPoints')}</Text> 
                    </View>
                    <Text style={{color:'#fff'}}>TOTAL SCORES: {data.getgame('totalPoints')}</Text>
                    {/* <Text style={{color:'#fff'}}>YOUR SCORE: {data.score}</Text> */}
                    <Text style={styles.txt}><Text >NUMBER OF SPINS</Text>: {data.numberOfSpins}</Text>
                    </>
                )
            }

        </TouchableOpacity>
    )
}





export default class History extends Component {

    viewLoaded=false;

    state={
        loading:false,
        history:[]
    }

    componentDidMount(){
        this.viewLoaded=true;
        this.gethistory();
    }

    componentWillUnmount(){
        this.viewLoaded=false;
    }

    gethistory=async()=>{
        try {
            this.setState({loading:true});
            const response=await service.getbets(this.user._id);
            if(!response.success){
                throw Error(response.message);
            };
            const history=response.data.map(d=>new BetModel(d));
            return this.viewLoaded && this.setState(
                {
                    loading:false,
                    history:history
                }
            )
        } catch (err) {
            this.viewLoaded && this.setState(
                {
                    loading:false
                }
            )
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <AuthConsumer>
                    {
                        ({state})=>{
                            this.user=state.user;
                        }
                    }
                </AuthConsumer>
                <GameConsumer>
                    {
                        ({dispath})=>{
                            this.dispatch=dispath;
                        }
                    }
                </GameConsumer>
                <CustomHeader title="History" />
                <FlatList
                    onRefresh={this.gethistory.bind(this)}
                    refreshing={this.state.loading}
                    ListEmptyComponent={()=>(
                        <Text style={{
                            margin:50,
                            textAlign:'center',
                            color:'#fff'
                        }}>Your history is empty!</Text>
                    )}
                    style={styles.scrollview} 
                    data={this.state.history}
                    keyExtractor={(v,i)=>i.toString()}
                    removeClippedSubviews={true} // Unmount components when outside of window 
                    initialNumToRender={10} // Reduce initial render amount
                    maxToRenderPerBatch={1} // Reduce number in each render batch
                    updateCellsBatchingPeriod={100} // Increase time between renders
                    windowSize={7} // Reduce the window size
                    renderItem={({item})=>{
                        return (
                            <HistoryItem 
                                data={item}
                                onPress={
                                    ()=>{
                                        switch (item.getgame('game_type')) {
                                            case 'RW':
                                                return !item.getgame('gameover') && this.props.navigation.navigate(
                                                    'WheelPage',
                                                    {data:{...item.gameId,reward:item.rewardId}}
                                                )
                                            default:
                                                return this.dispatch(
                                                    {...item.gameId,...item},
                                                    () =>{
                                                        this.props.navigation.navigate(
                                                            'Slots'
                                                        )
                                                    }
                                                )
                                        }
                                    }
                                }
                            />
                        )
                    }}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.yellow,
    },
    amount: { 
        borderRadius: 30, 
        backgroundColor: 'white',
        paddingLeft:10,
        paddingRight:10,
        paddingTop:5,
        paddingBottom:5,
        marginRight:10
    },
    txt: { color: '#ffffff' },
    header: {
        fontSize: 35,
        color: 'white',
        marginLeft: 22,
        marginBottom: 10,
        alignSelf: 'flex-start'
    },
    scrollview: {
        width: '100%'
    },
    historyItem: {
        paddingHorizontal: '10%',
        width: '100%',
        justifyContent: 'space-between',
        marginVertical: 10,
        padding: '5%',
        borderColor: '#fff',
        borderBottomWidth: 1,
    },
    status:{
        borderRadius:50,
        borderColor:'#eee',
        borderWidth:0.5,
        justifyContent:'center',
        alignItems:'center',
        paddingLeft:10,
        paddingRight:10,
        paddingTop:5,
        paddingBottom:5
    }
})