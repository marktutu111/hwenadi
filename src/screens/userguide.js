import React, { Component } from 'react'
import { StyleSheet,ScrollView, View, Text, Image, TouchableOpacity } from 'react-native';
import colors from '../constants/colors';
import Swiper from 'react-native-swiper';
import { AuthConsumer } from '../store/auth.state';
import LogoContainer from "../components/logo/logo";
import AsyncStorage from '@react-native-async-storage/async-storage';





export default class PrivacyPolicy extends Component {
    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <AuthConsumer>
                    {
                        ({state})=>{
                            const user=state.user;
                            return(
                                <Swiper 
                                    style={styles.wrapper} 
                                    showsButtons={false}
                                    dotColor={'#fff'}
                                    activeDotColor={'red'}
                                    loop={false}
                                >
                                    <View style={styles.slide1}>
                                        <LogoContainer source={require('../assets/images/LOGO_MELCOM.png')}/>
                                        <Text style={styles.text}>Welcome {user.name}</Text>
                                        <Text style={styles.small}>your guide on how to Play and Win big.</Text>
                                    </View>
                                    <View style={styles.slide1}>
                                        <LogoContainer source={require('../assets/images/1.png')}/>
                                        <Text style={styles.text}>Search your desired product or choose from the list displayed on the Home screen.</Text>
                                    </View>
                                    <View style={styles.slide1}>
                                    <LogoContainer source={require('../assets/images/2.png')}/>
                                        <Text style={styles.text}>Tap on the product to reveal the slots drawer.Each slot has a price tag 
to tell you how much you have to spend per slot. you cannot choose a slot that has been 
chosen already.</Text>
                                    </View>
                                    <View style={styles.slide1}>
                                    <LogoContainer source={require('../assets/images/3.png')}/>
                                        <Text style={styles.text}>
                                        Behind one of the slots is an indication of a win which is generated randomly by the system hence the 
lucky winner is revealed after all the slots have been purchased
                                        </Text>
                                    </View>
                                    <View style={styles.slide1}>
                                    <LogoContainer source={require('../assets/images/4.png')}/>
                                        <Text style={styles.text}>
                                            Choose one or more slots you want to make payment for by tapping on them.
                                        </Text>
                                    </View>
                                    <View style={styles.slide1}>
                                    <LogoContainer source={require('../assets/images/5.png')}/>
                                        <Text style={styles.text}>
                                            Tap on the Done button and proceed to the payment screen. Enter your mobile money wallet number and choose your network.
                                        </Text>
                                    </View>
                                    <View style={styles.slide1}>
                                    <LogoContainer source={require('../assets/images/6.png')}/>
                                        <Text style={styles.text}>
                                            An sms with an OTP is sent to the provided mobile money wallet number.
                                            Enter the 5 digit code received to confirm your request.
                                        </Text>
                                    </View>
                                    <View style={styles.slide1}>
                                    <LogoContainer source={require('../assets/images/7.png')}/>
                                        <Text style={styles.text}>
                                            Wait for your mobile money provider to send you a prompt to authorize the payment. Enter your mobile money pin and wait for Tonadi to send you an sms confirming your reservation and transaction code.
                                        </Text>
                                    </View>
                                    <View style={styles.slide1}>
                                        <LogoContainer source={require('../assets/images/8.png')}/>
                                        <Text style={styles.text}>
                                            Now wait till all the slots are purchased or buy more if you still want to. You will be notified by sms when you win or loose. You can also check the history screen to see the status of your reservations and the lucky slot that won.
                                        </Text>
                                    </View>
                                    <View style={styles.slide1}>
                                        <TouchableOpacity onPress={
                                            ()=>{
                                                AsyncStorage.setItem('showguide','NO').then(
                                                    ()=>{
                                                        this.props.navigation.navigate(
                                                            'MainApp'
                                                        )
                                                    }
                                                ).catch(err=>null);
                                            }
                                        }>
                                            <LogoContainer 
                                                width={180}
                                                height={180}
                                            source={require('../assets/images/TAP.png')}/>
                                        </TouchableOpacity>
                                    </View>
                                </Swiper>
                            )
                        }
                    }
                </AuthConsumer>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:colors.yellow,
        alignItems:'center'
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:colors.yellow
      },
      text: {
        color: '#fff',
        fontSize: 25,
        fontWeight: 'bold',
        margin: 15,
      },
      small:{
          color:'#fff'
      },
      numbers:{
          color:'#fff',
          fontSize:20
      }
})