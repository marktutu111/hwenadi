import React, { Component, Fragment } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import CustomHeader from '../components/header/header';
import service from "../services/bet.service";
import moment from "moment";

class ParticipantsPage extends Component {

    isViewLoaded=false;

    state={
        data:[],
        loading:false
    }

    componentDidMount(){
        this.viewLoaded=true;
        const {id}=this.props.route.params;
        this.getbets(id);
    }

    componentWillUnmount(){
        this.viewLoaded=false;
    }

    getbets=async(id)=>{
        try {
            this.setState({loading:true});
            const response=await service.getbetParticipants(id);
            if(!response.success){
                throw Error(response.message);
            }
            return this.viewLoaded && this.setState(
                {
                    loading:false,
                    data:response.data
                }
            )
        } catch (err) {
            this.viewLoaded && this.setState(
                {
                    loading:false
                }
            )
        }
    }


    render() {
        return (
            <Fragment>
                    <CustomHeader 
                        showBackButton
                        color={'#000'}
                        title="Hunters"
                        onPressOut={
                            ()=>this.props.navigation.goBack()
                        }
                    />
                <FlatList
                    onRefresh={this.getbets.bind(this)}
                    refreshing={this.state.loading}
                    ListEmptyComponent={()=>(
                        <Text style={{
                            margin:50,
                            textAlign:'center',
                            color:'#000'
                        }}>No participant yet!</Text>
                    )}
                    style={styles.scrollview} 
                    data={this.state.data}
                    keyExtractor={(v,i)=>i.toString()}
                    removeClippedSubviews={true} // Unmount components when outside of window 
                    initialNumToRender={10} // Reduce initial render amount
                    maxToRenderPerBatch={1} // Reduce number in each render batch
                    updateCellsBatchingPeriod={100} // Increase time between renders
                    windowSize={7} // Reduce the window size
                    renderItem={({item})=>{
                        switch (item.gameId?.game_type) {
                            case 'RW':
                                return (
                                    <View style={styles.listItem}>
                                        <Text style={{fontSize:20,marginBottom:5}}>{item.customerId?.name}</Text>
                                        <Text>POINTS: {item.score}</Text>
                                        <Text>DATE: { moment(item.updatedAt).fromNow() }</Text>
                                        <Text>SPINS:{item.numberOfSpins}</Text>
                                    </View>
                                )
                            default:
                                return (
                                    <View style={styles.listItem}>
                                        <Text style={{fontSize:20,marginBottom:5}}>{item.customerId?.name}</Text>
                                        <Text>SLOTS: {item.slotNumber?.length}</Text>
                                        <Text>PURCHASE DATE: { moment(item.updatedAt).fromNow() }</Text>
                                        <Text>AMOUNT PAID: GHS{item.gameId?.betPrice*item.slotNumber?.length}</Text>
                                    </View>
                                )
                        }
                    }}
                />
            </Fragment>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: '10%',
        alignItems: 'center'
    },
    backBtn:{position:'absolute',alignSelf:'flex-start',marginLeft:10 },
    text: {
        alignSelf: 'flex-start',
        fontSize: 30,
        marginVertical:10
    }, header: {
        fontSize: 28,
        textAlign: 'center',
        alignSelf:'center'
    },
    listItem:{
        padding:20,
        borderBottomColor:'#ddd',
        borderBottomWidth:1
    },
    float:{
        flexDirection:'row',
        alignItems:'center'
    }
})
export default ParticipantsPage;
