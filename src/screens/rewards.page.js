import React, { Component } from 'react'
import { View, StyleSheet, ScrollView ,Platform, ActivityIndicator, Image, Dimensions, ImageBackground} from 'react-native';
import service from "../services/gameplays.service";
import { GameConsumer } from "../store/gameplay.state";
import colors from '../constants/colors';
import CustomHeader from '../components/header/header';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Reward from '../components/reward/reward.component';
const { height,width } = Dimensions.get('window');
const _height = height * 0.66;




export default class RewardsPage extends Component {

    viewLoaded=false;
    interval=null;

    state={
        page:1,
        loading:false,
        onfilter:false
    }


    componentDidMount() {
        this.viewLoaded=true;
        this.getgames();
        this.interval=setInterval(()=>{
            this.getgames();
        }, 1000*10);
    };


    componentWillUnmount(){
        clearInterval(this.interval);
    }


    async getgames(p=false) {
        try {
            const page=p?this.state.page:1;
            this.viewLoaded && this.setState({ loading:true });
            const response = await service.paginate(
                {
                    filter:{
                        active:true,
                        gameover:false,
                        game_type:'RW'
                    },
                    page:page,
                    limit:10
                }
            );
            if (!response.success) {
                throw Error(response.message);
            }
            const games=response.data;
            const data=p?[...this.games,...games]:games;
            return this.viewLoaded && this.dispath(
                {
                    rewards:data
                },
                ()=>this.setState(
                    {
                        loading:false,
                        page:games.length>10?page+1:page
                    }
                )
            )
        } catch (err) {
            this.viewLoaded && this.setState(
                { 
                    loading:false 
                }
            );
        }
    }

    


    render() {

        const isCloseToBottom=({layoutMeasurement, contentOffset, contentSize})=>{
            const paddingToBottom = 20;
            return layoutMeasurement.width + contentOffset.x >=
              contentSize.width - paddingToBottom;
        };

        return (
            <View style={styles.container}>
                <CustomHeader 
                    title="Tonadi"
                    onSearch={()=>this.props.navigation.navigate('Product-Search')}
                    search={true}
                    showLogo={true}
                    onPressOut={
                        ()=>{
                            this.dispath(
                                {
                                    onfilter:false
                                },
                                ()=>this.getgames(false).catch(err=>null)
                            )
                        }
                    }
                />
                <GameConsumer>
                    {
                        ({ dispath,state }) => {
                            this.dispath=dispath;
                            this.games=state.rewards;
                            return (
                                <ScrollView
                                    horizontal
                                    decelerationRate='fast'
                                    bounces={false}
                                    snapToInterval={270}
                                    showsVerticalScrollIndicator={false}
                                    showsHorizontalScrollIndicator={false}
                                    style={{ alignSelf: 'center', height: '100%',marginTop:50 }}
                                    scrollEventThrottle={400}
                                    onScroll={({nativeEvent})=>{
                                        if (isCloseToBottom(nativeEvent)) {
                                            this.getgames(true);
                                        }
                                    }}
                                >
                                    <View style={{ height: '100%', width: 45 }}></View>
                                    { this.state.loading && this.games.length===0 && <View style={{
                                        justifyContent:'center',
                                        alignItems:'center'
                                    }}>
                                        <ActivityIndicator
                                            size={'large'}
                                            color='#fff'
                                        />
                                    </View>}
                                    {
                                        this.games.map((game,i)=>game['game_type']==='RW' && <Reward 
                                            {...game}
                                            key={i.toString()}
                                            goTo={()=>this.props.navigation.navigate(
                                                'WheelPage',
                                                {data:game}
                                            )}
                                        />)
                                    }
                                </ScrollView>
                            )
                        }
                    }
                </GameConsumer>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.yellow,
    },
    header: {
        fontSize: 35,
        color: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 35,
        marginLeft: 22,
        marginRight: 22,
        marginBottom: 40,
    },
    headerText: {
        fontSize: 35,
        color: 'white',
    },
    filter:Platform.select(
        {
            'ios':{
                marginRight:20
            }
        }
    ),
    imageContainer:{
        height:_height,
        width: width / 1.3,
        backgroundColor: 'white',
        marginRight: 20,
        borderRadius:20,
        alignItems: 'center',
        justifyContent: 'space-around',
        overflow: 'hidden',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    }
})