import React, { Component, Fragment } from 'react'
import { View, StyleSheet, TouchableOpacity, Alert, Dimensions,ScrollView,StatusBar } from 'react-native'
import NText from '../components/text'
import NButton from '../components/buttons/buttons'
import NTextInput from '../components/text-input'
import Icon from 'react-native-vector-icons/Ionicons';
import CustomHeader from '../components/header/header';


import otpservice from '../services/otp.service';
import { GameConsumer } from '../store/gameplay.state';
import betservice from "../services/bet.service";
import depositService from "../services/debosit.service";
const { height } = Dimensions.get('window');



export default class VerifyPayment extends Component {

    viewLoaded=false;

    state={
        loading: false,
        otp: ''
    }

    componentDidMount() {
        this.viewLoaded=true;
        this.setState(
            {
                ...this.state,...this.props.route.params
            },()=>this.sendOtp()
        )
    }

    async deposit(){
        try {
            const { otp }=this.state;
            if (otp === '') return;
            this.setState({ loading: true });
            const data={ ...this.state.data,otp:otp };
            const response = await depositService.deposit(data);
            if(response===null){
                throw Error(
                    `Dear user,we cannot process your request now check your internet connection`
                )
            }
            if (!response.success) {
                throw Error(
                    response.message
                )
            }
            return this.viewLoaded && this.setState(
                {
                    loading: false
                }, ()=>{
                    this.props.navigation.navigate('Profile');
                    Alert.alert(
                        'Dear Customer',
                        response.message
                    )
                }
            )
        } catch (err) {
            Alert.alert(
                'Dear Customer',
                err.message
            )
            this.viewLoaded && this.setState(
                {
                    loading: false
                }
            )
        }
    }


    async pay() {
        try {
            const { otp }=this.state;
            if (otp === '') return;
            this.setState({ loading: true });
            const data={ ...this.data, otp: otp };
            const response = await betservice.bet(data);
            if(response===null){
                throw Error(
                    `Dear user,we cannot process your request now check your internet connection`
                )
            }
            if (!response.success) {
                throw Error(response.message)
            }
            return this.viewLoaded && this.setState(
                {
                    loading: false
                }, ()=>{
                    this.dispath(
                        {
                            selected_slots:[]
                        }
                    )
                    this.props.navigation.navigate('MainApp');
                    Alert.alert(
                        'Dear Customer',
                        response.message
                    )
                }
            )
        } catch (err) {
            Alert.alert(
                'Dear Customer',
                err.message
            )
            this.viewLoaded && this.setState(
                {
                    loading: false
                }
            )
        }
    }

    async sendOtp(resend=false) {
        try {
            if (resend && this.viewLoaded) {
                this.setState(
                    {
                        loading: true
                    }
                )
            }
            let phoneNumber='';
            switch (this.state.page) {
                case 'DEPOSIT':
                    phoneNumber=this.state.data[
                        'accountNumber'
                    ];
                    break;
                default:
                    phoneNumber=this.data.accountNumber
                    break;
            }
            const response = await otpservice.sendOtp(
                { 
                    phoneNumber:phoneNumber 
                }
            );
            if(response===null){
                throw Error(
                    `Dear user,we cannot process your request now check your internet connection`
                )
            }
            if (!response.success) {
                throw Error(
                    response.message
                );
            }
            this.viewLoaded && this.setState(
                {
                    loading:false
                },()=>{
                    Alert.alert('OTP', 'Verification code has been sent to your provided mobile money number.');
                }
            )
        } catch (err) {
            this.viewLoaded && this.setState(
                {
                    loading:false
                },()=>{
                    Alert.alert('OTP',err.message);
                }
            )
        }
    }

    render() {
        return (
            <GameConsumer>
                {
                    ({ state,dispath }) => {
                        this.data = state.bet;
                        this.dispath=dispath;
                        return (
                            <View style={{backgroundColor:'#fff'}}>

                                <StatusBar barStyle="dark-content"/>
                                <CustomHeader 
                                    showBackButton
                                    color={'#000'}
                                    title="OTP"
                                    onPressOut={
                                        ()=>this.props.navigation.goBack()
                                    }
                                />

                                <ScrollView 
                                    contentContainerStyle={styles.container} 
                                    keyboardShouldPersistTaps='handled'
                                    showsVerticalScrollIndicator={false}
                                >

                                    <NText style={{ color: '#000',padding: 20, }}>We are sending you a verification code</NText>
                                    <NTextInput 
                                        onChangeText={v => this.state.otp = v.trim()} 
                                        placeholder="OTP" 
                                        width='100%' 
                                        _style={{backgroundColor:'#ddd'}}
                                        keyboardType='number-pad' 
                                        maxLength={5}
                                    />
                                    <NButton 
                                        onPress={()=>{
                                            switch (this.state.page) {
                                                case 'DEPOSIT':
                                                    return this.deposit();
                                                default:
                                                    return this.pay();
                                            }
                                        }} 
                                        loading={this.state.loading} 
                                        title='PAY' 
                                        width='100%' 
                                    />
                                    <View style={styles.row}>
                                        <View 
                                            style={{
                                                flexDirection:'row',
                                                alignItems:'center'
                                            }}
                                        >
                                            <Icon 
                                                name='chatbubble-ellipses-sharp' 
                                                size={25} color='black' 
                                            />
                                            <TouchableOpacity
                                                disabled={this.state.loading}
                                                onPress={()=>this.sendOtp(true)}>
                                                <NText style={{ color: 'black' }}> Resend SMS</NText>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        )
                    }
                }
            </GameConsumer>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 36,
        paddingBottom: 30,
        height: height * 0.85,
        alignItems: 'center'
    },
    header: {
        marginBottom: 25,
        fontSize: 35,
        marginLeft: 22,
        alignSelf: 'flex-start'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
        marginVertical: 20
    }
})