import React from 'react'
import { Text } from 'react-native'
const NText = (props) =><Text style={[props.style]}>{props.children}</Text>
export default NText