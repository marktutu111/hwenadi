import React, { Component, Fragment } from 'react';
import { View, Text, StyleSheet, FlatList,Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import CustomHeader from '../components/header/header';
import service from "../services/bet.service";
import moment from "moment";
import colors from '../constants/colors';

class WinnersPage extends Component {

    isViewLoaded=false;

    state={
        data:[],
        loading:false
    }

    componentDidMount(){
        this.viewLoaded=true;
        this.getbets();
    }

    componentWillUnmount(){
        this.viewLoaded=false;
    }

    getbets=async()=>{
        try {
            this.setState({loading:true});
            const response=await service.getWinners();
            if(!response.success){
                throw Error(response.message);
            }
            return this.viewLoaded && this.setState(
                {
                    loading:false,
                    data:response.data
                }
            )
        } catch (err) {
            this.viewLoaded && this.setState(
                {
                    loading:false
                }
            )
        }
    }


    render() {
        return (
            <View style={styles.container}>
                    <CustomHeader 
                        color={'#fff'}
                        title="Winners"
                    />
                <FlatList
                    onRefresh={this.getbets.bind(this)}
                    refreshing={this.state.loading}
                    ListEmptyComponent={()=>(
                        <Text style={{
                            margin:50,
                            textAlign:'center',
                            color:'#fff'
                        }}>No participant yet!</Text>
                    )}
                    style={styles.scrollview} 
                    data={this.state.data}
                    keyExtractor={(v,i)=>i.toString()}
                    removeClippedSubviews={true} // Unmount components when outside of window 
                    initialNumToRender={10} // Reduce initial render amount
                    maxToRenderPerBatch={1} // Reduce number in each render batch
                    updateCellsBatchingPeriod={100} // Increase time between renders
                    windowSize={7} // Reduce the window size
                    renderItem={({item})=>{
                        const game_type=item.gameId?.game_type;
                        return (
                            <View style={styles.listItem}>
                                {game_type==='PD'?(<View>
                                    <View style={{
                                        flexDirection:'row',
                                        alignItems:'center',
                                        marginVertical:10,
                                    }}>
                                        <Icon style={{marginRight:10}} name="trophy" color={'#fff'} size={20}/>
                                        <Text style={{fontSize:20,color:'#fff'}}>{item.customerId?.name}</Text>
                                    </View>
                                    <Text style={{color:'#fff',fontSize:20}}>{item.productId?.name}</Text>
                                    <Text style={{color:'#fff'}}>SLOTS: {item.slotNumber?.length}</Text>
                                    <Text style={{color:'#fff'}}>START DATE: { moment(item.gameId?.createdAt).format('MM-DD-YYYY') }</Text>
                                    <Text style={{color:'#fff'}}>END DATE: { moment(item.gameId?.updatedAt).format('MM-DD-YYYY') }</Text>
                                    <Text style={{color:'#fff'}}>AMOUNT PAID: GHS{item.gameId?.betPrice*item.slotNumber?.length}</Text>
                                    <Text style={{color:'#fff'}}>WINNING SLOT: {item.gameId?.winningSlot}</Text>
                                </View>):(
                                    <View>
                                    <View style={{
                                        flexDirection:'row',
                                        alignItems:'center',
                                        marginVertical:10,
                                    }}>
                                        <Icon style={{marginRight:10}} name="trophy" color={'#fff'} size={20}/>
                                        <Text style={{fontSize:20,color:'#fff'}}>{item.customerId?.name}</Text>
                                    </View>
                                    <Text style={{color:'#fff',fontSize:20}}>{item.rewardId?.name}</Text>
                                    <Text style={{color:'#fff'}}>SCORE: {item.score}</Text>
                                    <Text style={{color:'#fff'}}>START DATE: { moment(item.gameId?.createdAt).format('MM-DD-YYYY') }</Text>
                                    <Text style={{color:'#fff'}}>WINNING SCORE: {item.gameId?.maxPoints}</Text>
                                </View>
                                )}
                                <View style={{
                                    width:100,
                                    height:110,
                                    justifyContent:'center',
                                    alignItems:'center',
                                    borderRadius:10,
                                    overflow:'hidden'
                                }}>
                                    {game_type==='PD'?<Image 
                                        style={{
                                            width:'100%',
                                            height:'100%'
                                        }}
                                        source={{uri:item.productId?.image}}
                                    />:<Image 
                                    style={{
                                        width:'100%',
                                        height:'100%'
                                    }}
                                    source={{uri:item.rewardId?.label}}
                                />}
                                </View>
                            </View>
                        )
                    }}
                />
            </View>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.yellow,
    },
    backBtn:{position:'absolute',alignSelf:'flex-start',marginLeft:10 },
    text: {
        alignSelf: 'flex-start',
        fontSize: 30,
        marginVertical:10
    }, header: {
        fontSize: 28,
        textAlign: 'center',
        alignSelf:'center',
    },
    listItem:{
        padding:20,
        borderBottomColor:'#fff',
        borderBottomWidth:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    float:{
        flexDirection:'row',
        alignItems:'center'
    }
})
export default WinnersPage;
