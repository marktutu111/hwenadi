import React from 'react';
import { View,Image } from 'react-native';

const LogoContainer = ({source,width,height}) => (
    <View style={{
        alignItems:'center',
        justifyContent:'center',
        width:width || 80,
        height:height || 80,
        backgroundColor:'geen',
        margin:20
    }}>
        <Image
            source={source}
            style={{
                width:'100%',
                height:'100%'
            }}
        />
    </View>
);

export default LogoContainer;
