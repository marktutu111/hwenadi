import { BASEURL } from "../api/api";

const bet=(data)=>fetch(
    `${BASEURL}/bets/create`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>null);

const getbet=(id)=>fetch(
    `${BASEURL}/bets/get/${id}`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        },
    }
).then(res=>res.json()).catch(err=>null);


const spinBet=(data)=>fetch(
    `${BASEURL}/bets/spin/create`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>null);

const getbets=(id)=>fetch(
    `${BASEURL}/bets/customer/get/${id}`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);

const getbetParticipants=(id)=>fetch(
    `${BASEURL}/bets/all/get/${id}`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);

const getWinners=(id)=>fetch(
    `${BASEURL}/bets/winners/get`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);


export default {
    bet,
    getbets,
    getbetParticipants,
    getWinners,
    spinBet,
    getbet
}