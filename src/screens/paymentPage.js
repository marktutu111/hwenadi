import React, { useState, Component, Fragment } from 'react'
import { View, Text, StyleSheet, FlatList, Keyboard, Dimensions, Alert, Platform,ScrollView,TouchableOpacity, KeyboardAvoidingView } from 'react-native'
import { Picker } from '@react-native-community/picker';
import NText from '../components/text'
import NTextInput from '../components/text-input'
import NButton from '../components/buttons/buttons'
import Icon from 'react-native-vector-icons/FontAwesome';
import CustomHeader from '../components/header/header';
import { CustomPicker } from 'react-native-custom-picker'


const { height } = Dimensions.get('window');

import { AuthConsumer } from "../store/auth.state";
import { GameConsumer } from '../store/gameplay.state';


class PaymentPage extends Component {

    isViewLoaded = false;

    state = {
        accountNumber:'',
        accountIssuer:'',
        amount: 0,
        slotNumber: [],
        loading: false
    }

    async pay() {
        try {
            const { accountIssuer, accountNumber } = this.state;
            if (accountNumber === '') {
                return Alert.alert(
                    'Oops!', 
                    'Please enter your mobile money number'
                );
            };

            if(accountIssuer === '' ){
                return Alert.alert(
                    'Oops!', 
                    'Please choose your mobile money provider or network'
                );
            }
            const data = {
                gameId: this.game._id,
                customerId: this.user._id,
                productId: this.game.productId._id,
                amount: this.state.amount,
                slotNumber: this.game.selected_slots,
                accountNumber: accountNumber,
                accountIssuer: accountIssuer
            };
            return this.dispath(
                {
                    bet:data
                }, () =>this.props.navigation.navigate(
                    'VerifyPayment',
                    {
                        page:'PAY'
                    }
                )
            )
        } catch (err) {

        }
    }

    render() {
        return (
            <Fragment>
                <AuthConsumer>
                    {
                        ({ state }) => {
                            this.user = state.user;
                        }
                    }
                </AuthConsumer>
                <GameConsumer>
                    {
                        ({ state, dispath }) => {
                            this.game = state;
                            this.dispath = dispath;
                            const {
                                betPrice,
                                selected_slots,
                                productId
                            } = state;
                            this.state = { ...this.state, amount: betPrice * selected_slots.length };
                            return (
                                <KeyboardAvoidingView behavior="padding" style={{height:'100%',backgroundColor: 'white'}}>
                                    <CustomHeader 
                                        showBackButton
                                        color={'#000'}
                                        title="Payment"
                                        onPressOut={
                                            ()=>this.props.navigation.goBack()
                                        }
                                    />
                                    <ScrollView
                                        keyboardShouldPersistTaps='handled'
                                        contentContainerStyle={{
                                            padding: 16,
                                            paddingBottom: 30,
                                            width: '100%'
                                        }}
                                    >
                                        <View style={[styles.container]}>
                                            <NText style={styles.text}>GHS{this.state.amount}</NText>
                                            <NText>{productId?.name}</NText>
                                            <NText>{selected_slots.length} slots</NText>
                                            <NTextInput
                                                maxLength={10}
                                                keyboardType='number-pad'
                                                placeholder='Mobile money number'
                                                width='100%'
                                                _style={{ backgroundColor: '#ddd', paddingLeft: 10 }}
                                                onChangeText={v => this.setState({ accountNumber: v.trim() })}
                                            />

                                            <View style={{ 
                                                    flexDirection: 'column', 
                                                    alignItems: 'center',
                                                    ...Platform.select(
                                                        {
                                                            'ios':{
                                                                margin:20
                                                            }
                                                        }
                                                    )
                                                }}
                                            >
                                                <NText style={{marginBottom:10}}>Choose network</NText>
                                                <CustomPicker
                                                    style={{ width:200,alignItems:'center' }}
                                                    placeholder={'Tap to pick...'}
                                                    options={[
                                                        {
                                                            label:"Mtn", 
                                                            value:"MTN"
                                                        },
                                                        {
                                                            label:"Vodafone", 
                                                            value:"VODAFONE"
                                                        },
                                                        {
                                                            label:"AirtelTigo", 
                                                            value:"AIRTELTIGO"
                                                        }
                                                    ]}
                                                    onValueChange={v=>{
                                                        if(!v){
                                                            this.setState(
                                                                {
                                                                    accountIssuer:''
                                                                }
                                                            )
                                                        }else{
                                                            this.setState(
                                                                { 
                                                                    accountIssuer:v.value 
                                                                }
                                                            )
                                                        }
                                                    }}
                                                    getLabel={item=>item.label}
                                                />
                                            </View>

                                            <NButton
                                                title='DONE'
                                                width={'60%'}
                                                onPress={this.pay.bind(this)}
                                            />
                                        </View>
                                    </ScrollView>
                                </KeyboardAvoidingView>
                            )
                        }
                    }
                </GameConsumer>

            </Fragment>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: '10%',
        alignItems: 'center'
    },
    backBtn:{position:'absolute',alignSelf:'flex-start',marginLeft:10 },
    text: {
        alignSelf: 'flex-start',
        fontSize: 30,
        marginVertical:10
    }, header: {
        fontSize: 28,
        textAlign: 'center',
        alignSelf:'center'
    }
})
export default PaymentPage
