import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Animated,
  Image,
  Text,
  ScrollView
} from 'react-native';
import NButton from '../buttons/buttons';
const {width,height} = Dimensions.get('screen');


class WheelComponent extends Component {
      

  constructor(props) {
    super(props);
    this.state = {
      enabled: false,
      started: false,
      finished: false,
      winner: null,
      gameScreen: new Animated.Value(width - 65),
      wheelOpacity: new Animated.Value(1),
      imageLeft: new Animated.Value(width / 2 - 30),
      imageTop: new Animated.Value(height / 2 - 70),
    };

    this.angle=0;
    this.prepareWheel();

  }



  prepareWheel=()=>{
    this.Rewards=this.props.options?.rewards;
    this.RewardCount=this.Rewards.length;
    this.numberOfSegments=this.RewardCount;
    this.fontSize=15;
    this.oneTurn=360;
    this.angleBySegment=this.oneTurn / this.numberOfSegments;
    this.angleOffset=this.angleBySegment / 1.2;
    this.winner=this.generateRandom();
    this._angle = new Animated.Value(0);

  };


  generateRandom(){
    let number=Math.floor(Math.random() * this.numberOfSegments);
    return number===10?number-1:number;
  }



  resetWheelState=()=>{
    this.setState({
      enabled: false,
      started: false,
      finished: false,
      winner: null,
      gameScreen: new Animated.Value(width - 40),
      wheelOpacity: new Animated.Value(1),
      imageLeft: new Animated.Value(width / 2 - 30),
      imageTop: new Animated.Value(height / 2 - 70),
    });
  };

  
  _tryAgain=()=>{
    this.prepareWheel();
    this.resetWheelState();
    this.angleListener();
    this._onPress();
  };



  angleListener=()=>{
    this._angle.addListener(event => {
      if (this.state.enabled) {
        this.setState({
          enabled: false,
          finished: false,
        });
      }

      this.angle=event.value;

    });
  };


  componentWillUnmount() {};


  componentDidMount() {
    this.angleListener();
    this.options=this.props.options || this.options;
  }

  _getWinnerIndex=()=>{
    const deg = Math.abs(Math.round(this.angle % this.oneTurn));
    // wheel turning counterclockwise
    if (this.angle < 0) {
      return Math.floor(deg / this.angleBySegment);
    }
    // wheel turning clockwise
    let num=((this.numberOfSegments - Math.floor(deg / this.angleBySegment)) %this.numberOfSegments);
     return num;
  };



  _onPress=()=>{
    const duration=this.props.options?.duration || 10000;
    this.setState({
      started: true,
    });
    Animated.timing(this._angle, {
      toValue:365 -this.winner * (this.oneTurn / this.numberOfSegments) + 360 * (duration / 1000),
      duration:duration,
      useNativeDriver:true,
    }).start(()=>{
      const winnerIndex=this._getWinnerIndex();
      this.setState({
        finished: true,
        winner:winnerIndex,
      },()=>this.props.onWinner(winnerIndex));
    });
  };


  _renderSvgWheel=()=>{
    return (
      <View style={styles.container}>
        {this._renderKnob()}
        <Animated.View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            transform: [
              {
                rotate: this._angle.interpolate({
                  inputRange: [-this.oneTurn, 0, this.oneTurn],
                  outputRange: [
                    `-${this.oneTurn}deg`,
                    `0deg`,
                    `${this.oneTurn}deg`,
                  ],
                }),
              },
            ],
            backgroundColor:'transparent',
            width: width - 150,
            height: width - 150,
            borderRadius:500,
            overflow:'hidden',
            borderWidth:2,
            borderColor:'#fff',
            opacity: this.state.wheelOpacity,
          }}>
          <View
            style={{
              transform: [{rotate:`-${this.angleOffset}deg`}],
              margin: 10,
              justifyContent:'center',
                alignItems:'center',
                overflow: 'hidden',
                width:270,
                height:270,
            }}
            >
            <Image 
                style={{
                    height:'94%',
                    width:'94%'
                }}
                source={this.props.options?.wheel}
            />
          </View>
        </Animated.View>
      </View>
    );
  };

  _renderKnob=()=>{
    const knobSize =20;
    const YOLO = Animated.modulo(
      Animated.divide(
        Animated.modulo(
          Animated.subtract(this._angle, this.angleOffset),
          this.oneTurn,
        ),
        new Animated.Value(this.angleBySegment),
      ),
      1,
    );

    return (
      <Animated.View
        style={{
          width: knobSize,
          height: knobSize * 2,
          justifyContent: 'flex-end',
          zIndex: 1,
          opacity:this.state.wheelOpacity,
          transform: [
            {
              rotate: YOLO.interpolate({
                inputRange: [-1, -0.5, -0.0001, 0.0001, 0.5, 1],
                outputRange: [
                  '0deg',
                  '0deg',
                  '35deg',
                  '-35deg',
                  '0deg',
                  '0deg',
                ],
              }),
            },
          ],
        }}>
        <View
          style={{
            transform: [{translateY: 15}],
            justifyContent:'center',
            alignItems:'center',
            width:27,
            height:(knobSize * 100) / 57
          }}>
          <Image
            source={require('../../assets/images/knob.png')}
            style={{ width:'100%', height: '100%' }}
          />
        </View>
      </Animated.View>
    );
  };


  render() {

    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };


    return (
      <View style={styles.container}>
        <Animated.View style={[styles.content, {padding: 10}]}>
          {this._renderSvgWheel()}
        </Animated.View>
        <Text style={{
            marginHorizontal:28,
            marginTop:10,
            color:'#fff'
        }}>Tap on the button to spin</Text>
        <NButton 
              onPress={this._tryAgain.bind(this)}
              loading={(this.state.started && !this.state.finished) || this.props.loading} 
              title='SPIN' 
              width='100%' 
              style={styles.btn}
          />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical:5,
    zIndex:1000000
  },
  content: {},
  startText: {
    fontSize: 50,
    color: '#fff',
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.4)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
  },
  btn:{
      marginVertical:50,
      width:'50%',
      marginRight:'auto',
      marginLeft:'auto',
      backgroundColor:'red',
      borderColor:'yellow'
  }
});



export default WheelComponent;