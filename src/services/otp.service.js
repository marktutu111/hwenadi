import { BASEURL } from "../api/api";

const sendOtp=(data)=>fetch(
    `${BASEURL}/otp/sendotp`,
    {
        method:'POST',
        body:JSON.stringify(data),
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);


export default {
    sendOtp
}