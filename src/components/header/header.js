import React, { Component } from 'react'
import { Text, View, StyleSheet,TouchableOpacity, Platform, SafeAreaView, Image } from 'react-native'
import NText from '../text';
import Icon from 'react-native-vector-icons/FontAwesome';


export default class CustomHeader extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={{
                    flexDirection:'row',
                    alignItems:'center',
                    marginLeft:20
                }}>
                    <TouchableOpacity
                        hitSlop={{
                            top:10,
                            left:10,
                            right:15,
                            bottom:10
                        }}
                        onPressOut={this.props.onPressOut}
                    >
                        {this.props.showBackButton && <Icon 
                            size={25} 
                            name="chevron-left" 
                            color={this.props.color || "#fff"}
                            style={{marginRight:15}}
                        />}
                        {this.props.showLogo && <View style={{
                            alignItems:'center',
                            justifyContent:'center',
                            width:40,
                            height:40,
                            backgroundColor:'geen',
                            marginRight:15
                        }}>
                            <Image
                                source={require('../../assets/images/LOGO_MELCOM.png')}
                                style={{
                                    width:'100%',
                                    height:'100%'
                                }}
                            />
                        </View>}
                    </TouchableOpacity>
                    <NText style={{...styles.header, color:this.props.color || '#fff',fontSize:this.props.headerFont || 25 }}>{this.props.title}</NText>
                </View>
                {this.props.search && <TouchableOpacity
                    style={{marginRight:20}}
                     hitSlop={{bottom:10,left:10,right:10,top:10}} 
                     onPress={this.props.onSearch} >
                    <Icon 
                        name={this.props.searchIcon || 'search'} 
                        size={25} 
                        color={ this.props.searchColor || 'white' } 
                    />
                </TouchableOpacity>}
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        ...Platform.select(
            {
                'android':{
                    marginTop:25
                },
                'ios':{
                    marginTop:50
                }
            }
        ),
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    header: {...Platform.select({
        'android':{
            fontSize: 35,
            color: 'white',
            // marginLeft: 22,
            marginBottom: 10,
            alignSelf: 'flex-start'
        },
        'ios':{
            fontSize: 35,
            color: 'white',
            alignSelf: 'flex-start'
        }
    }),marginTop:5}
});