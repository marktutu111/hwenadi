import React, { Component,Fragment } from 'react'
import { View, StyleSheet, Alert,KeyboardAvoidingView,ScrollView } from 'react-native'
import NButton from '../components/buttons/buttons'
import NTextInput from '../components/text-input'
import otpservice from '../services/otp.service';
import { AuthConsumer } from '../store/auth.state';
import CustomHeader from '../components/header/header';
import colors from '../constants/colors'


export default class ResetPassword extends Component {

    viewLoaded=false;

    state={
        loading:false,
        phone:'',
        password:'',
        confirm:''
    }

    componentDidMount(){
        this.viewLoaded=true;
    }

    async reset(){
        try {
            const {phone,password,confirm}=this.state;
            if(phone===''||password===''||confirm===''){
                throw Error(
                    'Please fill all required fields'
                )
            }
            if(password!==confirm){
                throw Error(
                    'Passwords does not match'
                )
            }
            this.props.navigation.navigate(
                'OTP',
                { data:this.state,page:'RP' }
            )
        } catch (err) {
            Alert.alert(
                'Oops!',
                err.message
            );
        }
    }

    render() {
        return (
            <View style={{backgroundColor:colors.yellow,height:'100%'}} >
                <AuthConsumer>
                    {
                        ({dispath})=>{
                            this.dispath=dispath;
                        }
                    }
                </AuthConsumer>
                <ScrollView 
                    contentContainerStyle={styles.container} 
                    keyboardShouldPersistTaps="handled"
                    showsVerticalScrollIndicator={false}
                >
                    <CustomHeader 
                        title="Reset Password"
                        showBackButton
                        onPressOut={()=>this.props.navigation.goBack()}
                    />
                    <View style={{width:'90%',alignItems:'center',marginTop:'10%'}}>
                        <NTextInput 
                            onChangeText={v=>this.state.phone=v.trim()} 
                            placeholder="Phone number" 
                            width='100%' 
                            keyboardType='number-pad'
                        />
                        <NTextInput 
                            onChangeText={v=>this.state.password=v.trim()} 
                            placeholder="New password" 
                            width='100%'
                            secureTextEntry={true}

                        />
                        <NTextInput 
                            onChangeText={v=>this.state.confirm=v.trim()} 
                            placeholder="Confirm password" 
                            width='100%'
                            secureTextEntry={true}
                        />
                        <NButton 
                            onPress={this.reset.bind(this)} 
                            loading={this.state.loading} 
                            title='SEND' 
                            width='100%' 
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.yellow,
        alignItems: 'center',
    },
    header: {
        marginBottom: 25,
        fontSize: 35,
        marginLeft: 22,
        alignSelf: 'flex-start'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width:'100%',
        marginVertical:20
    }
})