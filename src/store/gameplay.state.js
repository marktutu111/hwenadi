import React, { Component, createContext } from 'react';

const Context=createContext();
export const GameConsumer=Context.Consumer;


export default class GamePlayState extends Component {

    state={
        games:[],
        rewards:[],
        selected_slots:[],
        slotNumber:[],
        odds:[],
        reservations:[],
        onsearch:false,
        filter:[],
        onfilter:false
    }

    dispath(data,callback){
        Object.keys(data).forEach(key=>{
            if(this.state.onfilter && key==='games'){
                return;
            }
            this.setState(
                {[key]:data[key]}
            )
        });
        if(callback)callback();
    }


    selectAll(slots,callback){
        this.setState(
            {
                selected_slots:slots
            },callback
        )
    }

    select (slot){
        let slots=this.state.selected_slots;
        if(slots.indexOf(slot) > -1){
            this.setState(
                {
                    selected_slots:slots.filter(_slot=>_slot!==slot)
                }
            )
        } else{
            this.setState(
                {
                    selected_slots:[
                        ...slots,
                        slot
                    ]
                }
            )
        }

    }

    clear(){
        this.setState({slotNumber:[],odds:[],reservations:[]})
    }


    get_taken(num){
        return this.state.slotNumber.includes(num) || this.state.reservations.includes(num) ? true:false;
    }

    get_selected_slot(number){
        return this.state.selected_slots.indexOf(number) > -1?true:false;
    }

    render() {
        return (
            <Context.Provider 
                value={{
                    state:this.state,
                    dispath:this.dispath.bind(this),
                    select:this.select.bind(this),
                    selectAll:this.selectAll.bind(this),
                    get_selected_slot:this.get_selected_slot.bind(this),
                    get_taken:this.get_taken.bind(this)
                }}
            >
                {this.props.children}
            </Context.Provider>
        )
    }
}
