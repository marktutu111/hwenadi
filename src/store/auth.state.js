import React, { Component, createContext } from 'react';
export const AuthProvider=createContext();
export const AuthConsumer=AuthProvider.Consumer;
import fetchIntercept from "fetch-intercept";
import {navigate} from "../services/navigation.service";
import { Alert } from "react-native";


export default class AuthState extends Component {

    state={
        user:null,
        token:null,
        showguide:null
    }

    dispath(data){
        Object.keys(data).forEach(key=>{
            this.setState(
                {[key]:data[key]}
            )
        })
    }

    setHeaders=(token)=>{
        this.unregister=fetchIntercept.register({
          request: (url,config)=>{
              config.headers={
                  'Content-type':'Application/json',
                  'Authorization': 'Bearer ' + token
              }
              return [url,config];
          },
          requestError:(err)=>{
              return Promise.reject(err);
          },
          response:(response)=>{
              if(response===null){
                  Alert.alert(
                      'No internet',
                      'Dear user,we cannot process your request now check your internet connection'
                  )
              }
              if(response.status===401){
                  this.unregister();
                  this.setState(
                      {
                          user:null
                      }
                  )
              }
              return response;
          },
          responseError:(err)=>{
              return Promise.reject(err);
          }
      })
  
    }

    render() {
        return (
            <AuthProvider.Provider 
                value={{
                    state:this.state,
                    dispath:this.dispath.bind(this),
                    setHeaders:this.setHeaders.bind(this)
                }}
            >
                {this.props.children}
            </AuthProvider.Provider>
        )
    }
}
