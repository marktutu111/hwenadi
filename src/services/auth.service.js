import { BASEURL } from "../api/api";

const login=(data)=>fetch(
    `${BASEURL}/customers/login`,
    {
        method:'POST',
        body:JSON.stringify(data),
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);


const signup=(data)=>fetch(
    `${BASEURL}/customers/new`,
    {
        method:'POST',
        body:JSON.stringify(data),
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);


const reset=(data)=>fetch(
    `${BASEURL}/customers/resetpassword`,
    {
        method:'POST',
        body:JSON.stringify(data),
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);


export default {
    login,
    signup,
    reset
}