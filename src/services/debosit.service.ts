import { BASEURL } from "../api/api";

const deposit=(data)=>fetch(
    `${BASEURL}/deposits/new`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>null);

const getDeposit=(id)=>fetch(
    `${BASEURL}/deposits/get/${id}`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);


export default {
    deposit,
    getDeposit
}