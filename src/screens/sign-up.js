import React, { Component } from 'react'
import { View, StyleSheet,ScrollView,Text, Alert,KeyboardAvoidingView } from 'react-native'
import NButton from '../components/buttons/buttons'
import NTextInput from '../components/text-input'
import colors from '../constants/colors'
import CustomHeader from '../components/header/header';
import { TouchableOpacity } from 'react-native-gesture-handler'



export default class Signup extends Component {

    state={
        phone:'',
        name:'',
        confirmpassword:'',
        password:''
    }

    send(){
        try {
            const data=this.state;
            if(data.name === '' || data.phone === ''){
                return;
            };
            if(data.confirmpassword !== data.password){
                throw Error(
                    'Passwords does not match'
                )
            }
            return Alert.alert(
                'Confirm Account',
                'By signing up you agree to our terms and conditions',
                [
                    {
                        text:'No',
                        onPress:null
                    },
                    {
                        text:'Yes',
                        onPress:()=>this.props.navigation.navigate('OTP', {data:this.state,page:'SP'})
                    }
                ]
            )
        } catch (err) {
            Alert.alert(
                'Oops',
                err.message
            )
        }
    }

    render() {
        return (
            <View style={{backgroundColor:colors.melcom_blue,height:'100%'}}>
                <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps='handled'>
                    <CustomHeader 
                        title="Sign up"
                        showBackButton
                        onPressOut={()=>this.props.navigation.goBack()}
                    />
                    <View style={{marginTop:30}}></View>
                    <NTextInput 
                        onChangeText={v=>this.state.name=v.trim()} 
                        placeholder="Name" 
                    />
                    <NTextInput 
                        keyboardType="phone-pad" 
                        maxLength={10}
                        onChangeText={v=>this.state.phone=v.trim()} 
                        placeholder="Phone number" 
                    />
                    <NTextInput 
                        onChangeText={v=>this.state.password=v.trim()} 
                        placeholder="Password" 
                        secureTextEntry={true}
                    />
                    <NTextInput 
                        onChangeText={v=>this.state.confirmpassword=v.trim()} 
                        placeholder="Confirm Password" 
                        secureTextEntry={true}
                    />
                    {/* <View style={{
                        marginTop:20,
                        marginBottom:10
                    }}>
                        <TouchableOpacity
                            onPress={
                                ()=>this.props.navigation.navigate(
                                    'Privacy-policy'
                                )
                            }
                        >
                            <Text>Terms and Conditions</Text>
                        </TouchableOpacity>
                    </View> */}
                    <NButton 
                        onPress={this.send.bind(this)} 
                        title='DONE' 
                        width='95%' 
                    />
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginVertical:10
    },
    header: {
        marginBottom: 25,
        fontSize: 35,
        marginLeft: 22,
        alignSelf: 'flex-start'
    }
})