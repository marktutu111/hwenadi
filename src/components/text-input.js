import React from 'react'
import {TextInput } from 'react-native'

const NTextInput = (props) => {
    return (
        <TextInput style={[{
            borderRadius: 5,
            padding: 10,
            width: '95%',
            height:60,
            marginVertical:5,
            backgroundColor:'white',
            fontSize:20
        },{...props._style}]} 
        enablesReturnKeyAutomatically={true} 
        placeholderTextColor="#bbb"
        placeholder={props.placeholder} {...props} 
        />
    )
}

export default NTextInput
