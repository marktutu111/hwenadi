import React,{PureComponent} from 'react'
import { StyleSheet, Text, View, TouchableOpacity,ImageBackground } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../constants/colors';
import NText from '../text'


class slotItem extends PureComponent {
    // props.state ->  selected  ||  null  ||  taken || takenByuser
    render(){
        const slot=this.props.slot;
        const selected_slot=this.props.get_selected_slot(slot);
        const taken_slot=this.props.get_taken(slot);
        const winning_slot=this.props.winningSlot;
        const gameover=this.props.gameover;

        const renderBg=()=>{
            return selected_slot || taken_slot ? '#FFC208':'#FFEC6C';
        };

        const bg=gameover && winning_slot===slot ? require('../../assets/images/WIN_MELCOM_BLUE.png'):require('../../assets/images/EMPTY_MELCOM_BLUE.png');

        const getBackground=()=>{
            switch (gameover) {
                case true:
                    if(slot===winning_slot){
                        return '#90ee90';
                    };
                    return renderBg();
                default:
                    return renderBg();
            }
        }

        return (
            <TouchableOpacity 
                disabled={taken_slot} 
                style={styles.touch} 
                onPress={()=>this.props.select(slot)}
            >
                <ImageBackground
                    source={bg}
                    style={[
                    styles.slot, { 
                        // backgroundColor:getBackground()
                    }]}
                >
                    <View style={{flexDirection:'row'}}>
                        {/* {taken_slot && <Icon name='check-circle' size={25} color={'#000'} style={{alignSelf:'flex-end'}}/>} */}
                        {selected_slot ? <Icon name='star' size={15} color={'white'}/>:null}
                    </View>

                    {
                        taken_slot && (winning_slot !== slot) && <Icon 
                        name='check-circle' 
                        size={40} 
                        color={'#fff'} 
                        style={{alignSelf:'center',position: 'absolute',
                        marginTop:40}}/>
                    }
                    {
                        !gameover && !taken_slot && <NText 
                            style={{
                                alignSelf:'center',
                                fontSize:15,
                                color:selected_slot?colors._yellow:'#fff',
                                position: 'absolute',
                                marginTop:50,
                            }}
                            >{this.props.number}
                        </NText>
                    }

                    {/* {taken_slot ? (
                        <Icon 
                            name='check-circle' 
                            size={40} 
                            color={'#fff'} 
                            style={{alignSelf:'center',position: 'absolute',
                            marginTop:40,}}/>
                        ):(<NText 
                            style={{
                                alignSelf:'center',
                                fontSize:20,
                                color:selected_slot?'white':'#000',
                                position: 'absolute',
                                marginTop:50,
                            }}
                            >{this.props.number}
                        </NText>)
                    } */}
                    <View style={styles.txtbg}>
                        <NText 
                            style={{color:selected_slot?colors._yellow:'#fff',fontWeight:'bold',fontSize:15}}
                        >{gameover && winning_slot === slot ? `slot ${this.props.number}`:`GHS${this.props.amount}` }</NText>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        )
    }
}

export default slotItem

const styles = StyleSheet.create({
    slot: {
        width: 120,
        height: 120,
        justifyContent: 'space-between',
        // backgroundColor: '#ccc',
        padding: 10,
        borderRadius: 16,
    },
    touch: { flex: 1, alignItems: 'center', margin: 10,position:'relative' },
    circle: {
        width: 10,
        height: 10,
        borderRadius: 5
    },
    txtbg:{
        backgroundColor:colors.yellow,
        maxWidth:100,
        padding: 4,
        borderRadius:10,
        position:'absolute',
        marginLeft:-15
    }
})
