import React from 'react'
import {TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native'
import NText from '../text'

const NButton = (props) => {
    return (
        <TouchableOpacity disabled={props.loading} onPress={props.onPress} style={[styles.container,{width:props.width},props.style]}>
            {
                props.loading ? (
                    <ActivityIndicator 
                        size={20}
                        color="#fff"
                    />
            ): <NText style={{color:'white'}}>{props.title}</NText>
            }
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'red',
        borderRadius: 5,
        alignItems:'center',
        justifyContent:'center',
        padding:8,
        margin:15,
        height:60,
        width:'100%'
    }
})
export default NButton
