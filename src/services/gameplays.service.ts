import { BASEURL } from "../api/api";

const getongoing=()=>fetch(
    `${BASEURL}/gameplay/ongoing/get`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);

const filterOngoing=(item)=>fetch(
    `${BASEURL}/gameplay/product/filter?item=${item}`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);


const getWinningSlot=(id)=>fetch(
    `${BASEURL}/gameplay/ws/get/${id}`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);

const paginate=(data)=>fetch(
    `${BASEURL}/gameplay/ongoing/paginate`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>null);


export default {
    getongoing,
    filterOngoing,
    paginate,
    getWinningSlot
}