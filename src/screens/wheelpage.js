import React, { Component, Fragment } from 'react';
import { View, Text, StyleSheet, FlatList,Image,Alert,TouchableOpacity,ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import CustomHeader from '../components/header/header';
import moment from "moment";
import colors from '../constants/colors';
import WheelComponent from '../components/wheelcomponent/wheelcomponent';
import { AuthConsumer } from '../store/auth.state';
import betService from '../services/bet.service';
import service from "../services/debosit.service";



class WheelOfFortunePage extends Component {

    isViewLoaded=false;

    state={
        loading:false,
        score:0,
        rewards:[
            '300',
            'SPIN AGAIN',
            '100',
            '300',
            'LOST',
            '400',
            '1000',
            '900',
            '700',
            '500',
            'JACKPOT',
            '100'
        ]
    }

    componentDidMount(){
        this.viewLoaded=true;
        this.setState(
            {
                ...this.state,...this.props.route.params[
                    'data'
                ]
            },()=>this.getDeposit()
        );
    }

    componentWillUnmount(){
        this.viewLoaded=false;
    }


    async getUserPoints(){
        try {
            this.setState(
                {
                    loading:true
                }
            );
            const response=await betService.getbet(
                this.state._id
            );
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.setState(
                {
                    loading:false,
                    score:response['data']['score']
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                }
            )
        }
    }


    async getDeposit(){
        try {
            this.setState(
                {
                    loading:true
                }
            );
            const {_id}=this.user;
            const response=await service.getDeposit(_id);
            if(response===null){
                throw Error(
                    'Charlie check your internet connection, you seems to be offline'
                )
            }
            if(!response.success){
                throw Error(
                    'You dont have points to play. Kindly make deposit'
                )
            };
            const {points}=response.data;
            if(points>0){
                this.setState(
                    {
                        loading:false,
                        points:points
                    },()=>this.getUserPoints()
                )
            }else throw Error('Insufficient points, kindly make a deposit');
        } catch (err) {
            this.setState(
                {
                    loading:false
                },()=>{
                    Alert.alert(
                        'Oops',
                        err.message
                    )
                }
            )
        }
    }


    async createSpin(score){
        try {
            const data={
                gameId:this.state._id,
                customerId:this.user._id,
                rewardId:this.state.reward?._id,
                score:score
            };
            const response=await betService.spinBet(data);
            if(response===null){
                throw Error(
                    'Charlie check your internet connection, you seems to be offline'
                )
            }
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.setState(
                {
                    points:response['points'],
                    score:this.state.score+score
                },()=>Alert.alert(
                    'yay!!',
                    `Your score is ${score}`
                )
            )
        } catch (err) {
            Alert.alert(
                'Oops',
                err.message
            )
        }
    }


    getScore(value){
        try {
            const rewards=this.state.rewards;
            if(parseInt(rewards[value])){
                const score=parseInt(rewards[value]);
                this.createSpin(
                    score
                );
            }else{
                throw Error(
                    rewards[value]
                )
            }
        } catch (err) {
            let message=err.message;
            if(message==='LOST'){
                Alert.alert('Oops','Charlie better luck next time, you lost a spin, try again');
            }else Alert.alert('Oops',message);;
        }
    }


    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <AuthConsumer>
                    {
                        ({state})=>{
                            this.user=state[
                                'user'
                            ]
                        }
                    }
                </AuthConsumer>
                <CustomHeader 
                    showBackButton
                    color={'#fff'}
                    title="Tressure Wheel"
                    onPressOut={this.props.navigation.goBack}
                />
                <View style={{
                    marginBottom:5,
                    marginLeft:20,
                    marginTop:10
                }}>
                    <Text style={{fontSize:25}}>{this.state.reward?.name}</Text>
                </View>
                <TouchableOpacity 
                    style={{
                        flexDirection:'row',
                        alignItems:'center',
                        marginBottom:5,
                        marginLeft:25,
                        marginTop:5,
                        width:'auto'
                    }}
                    onPress={()=>this.props.navigation.navigate('Participants',{ id:this.state._id })}
                >
                    <Icon 
                        name="user"
                        size={12}
                        color={'#fff'}
                        style={{marginRight:5}}
                    />
                    <Text style={{fontSize:17,color:'#fff'}}>View tressure hunters</Text>
                    <Icon 
                        name="chevron-right"
                        size={15}
                        color={'#fff'}
                        style={{ marginLeft:5 }}
                    />
                </TouchableOpacity>
                <View style={{
                    // marginVertical:10,
                    marginHorizontal:20,
                    flexDirection:'row',
                    justifyContent:'space-between'
                }}>
                    <View style={{
                        flexDirection:'row',
                        alignItems:'center',
                        marginVertical:2}}
                    >
                        <Icon name="coins" color="#fff" size={20}/>
                        <Text style={{
                            color:'#fff',
                            marginLeft:5}}
                        >POINTS: {this.state.points}</Text>
                    </View>
                    <View style={{
                        flexDirection:'row',
                        alignItems:'center',
                        marginVertical:2}}
                    >
                        <Icon name="award" color="#fff" size={20}/>
                        <Text style={{color:'#fff',marginLeft:5}}>SCORE: {this.state.score}</Text>
                    </View>
                </View>
                <WheelComponent
                    onWinner={(v)=>this.viewLoaded && this.getScore(v)}
                    loading={this.state.loading}
                    options={{
                        rewards:this.state.rewards,
                        duration:4000,
                        wheel:require('../assets/images/wheel.png')
                    }}
                />
            </ScrollView>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.yellow,
    },
    backBtn:{position:'absolute',alignSelf:'flex-start',marginLeft:10 },
    text: {
        alignSelf: 'flex-start',
        fontSize: 30,
        marginVertical:10
    }, header: {
        fontSize: 28,
        textAlign: 'center',
        alignSelf:'center'
    },
    listItem:{
        padding:20,
        borderBottomColor:'#fff',
        borderBottomWidth:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    float:{
        flexDirection:'row',
        alignItems:'center'
    }
})
export default WheelOfFortunePage;
